//
//  Schedule.swift
//  Switch TV
//
//  Created by Switch TV Kenya on 15/02/2019.
//  Copyright © 2019 Switch TV. All rights reserved.
//

import Foundation
import UIKit

class Schedule {
    
    // {"ScheduleResult":"[{\"SchID\":\"1\",\"SchDOW\":\"Mon\",\"SchTime\":\"0500-0600\",\"SchShow\":\"MORNING CAFÉ\",\"SchDOWID\":\"1\",\"SchShowID\":\"27\"}
    
    //MARK: Properties
    var schdlId: Int
    var DayOfWeek: String
    var schdlTime: String
    var schdlShow: String
    var schDOWId: Int
    var schShowId: Int
    // var photo: UIImage?
    
    init?(schdlId: Int, DayOfWeek: String, schdlTime: String, schdlShow: String, schDOWId: Int,  schShowId: Int) {
        
        // Initialization should fail if there is no name or if the rating is negative.
        /*   if videoId == 0 || categoryId == 0 || showId == 0 || thumb.isEmpty  {
         return nil
         } */
        
        // Initialize stored properties.
        self.schdlId = schdlId
        self.DayOfWeek = DayOfWeek
        self.schdlTime = schdlTime
        self.schdlShow = schdlShow
        self.schDOWId = schDOWId
        self.schShowId = schShowId
        
    }
    
}
