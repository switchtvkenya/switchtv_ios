//
//  SettingsViewController.swift
//  Switch TV
//
//  Created by Switch TV Kenya on 03/04/2019.
//  Copyright © 2019 Switch TV. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {
    @IBOutlet weak var settingTableView: UITableView!
    @IBOutlet weak var backImageView: UIImageView!
    
    let titles = ["About Switch TV","Follow us","Share this App"]
    let icons = ["info","follow us","share"]
    let nexticons = ["click","click","click"]
    let subtitles = ["www.switchtv.ke","Facebook | Twitter | Instagram","WhatsApp | Telegram | iMessage"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        settingTableView.delegate = self
        settingTableView.dataSource = self
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        backImageView.isUserInteractionEnabled = true
        backImageView.addGestureRecognizer(tapGestureRecognizer)

    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let tappedImage = tapGestureRecognizer.view as! UIImageView
        
        // Your action
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "FollowSegue_Stt" {
            let followVC = segue.destination as! FollowusViewController
        }
        else if segue.identifier == "AboutSegue_Stt" {
            let abtVC = segue.destination as! AboutusViewController
        }
    }

}


extension SettingsViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsTableViewCell") as! SettingsTableViewCell
        
        cell.setStt(icon: UIImage(named: icons[indexPath.row])!, title: titles[indexPath.row], iconNext: UIImage(named: nexticons[indexPath.row])!,subtitle: subtitles[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            performSegue(withIdentifier: "AboutSegue_Stt", sender: nil)
        }
        else if indexPath.row == 1 {
            performSegue(withIdentifier: "FollowSegue_Stt", sender: nil)
        }
        else if indexPath.row == 2 {
            // text to share
            let text = "https://itunes.apple.com/ke/app/switch-tv-kenya/id1453808388?mt=8"
            
            // set up activity view controller
            let textToShare = [ text ]
            
            let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
            
            activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
            
            // exclude some activity types from the list (optional)
            activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
            
            // present the view controller
            self.present(activityViewController, animated: true, completion: nil)
            
            /* share image
             // image to share
             let image = UIImage(named: "Image")
             
             // set up activity view controller
             let imageToShare = [ image! ]
             let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
             activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
             
             // exclude some activity types from the list (optional)
             activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
             
             // present the view controller
             self.present(activityViewController, animated: true, completion: nil)
 */
        }
    }
    
}
