//
//  Utiliity.swift
//  Switch TV
//
//  Created by Switch TV Kenya on 08/02/2019.
//  Copyright © 2019 Switch TV. All rights reserved.
//

import Network

class Utiliity
{

func checkInternet() -> String
{
    let monitor = NWPathMonitor()
    var check = ""
    
    monitor.pathUpdateHandler = { path in
        if path.status == .satisfied {
            check = "We're connected!"
            print("We're connected!")
            
        } else {
            print("No connection.")
            check = "No connection."
        }
        
      //  print(path.isExpensive)
    }
    
    let queue = DispatchQueue(label: "Monitor")
    monitor.start(queue: queue)
    
    return check
}
}
