//
//  ScheduleEntity.swift
//  Switch TV
//
//  Created by Switch TV Kenya on 20/02/2019.
//  Copyright © 2019 Switch TV. All rights reserved.
//

import Foundation
import SQLite

class ScheduleEntity{
    
    static let shared = ScheduleEntity()
    
    private let tblSchedule = Table("Schedule")
    
    private let id = Expression<Int64>("id")
    private let DayOfWeek = Expression<String>("DayOfWeek")
    private let schdlTime = Expression<String>("schdlTime")
    private let schdlShow = Expression<String>("schdlShow")
    private let schDOWId = Expression<Int64>("schDOWId")
    private let schShowId = Expression<Int64>("schShowId")
    
    private init()
    {
        // create table if doesn't exist
        do{
            if let connection = DBHelper.shared.connection {
                try connection.run(tblSchedule.create(temporary: false, ifNotExists: true, withoutRowid: false, block: { (table) in
                    table.column(self.id, primaryKey: true)
                    table.column(self.DayOfWeek)
                    table.column(self.schdlTime)
                    table.column(self.schdlShow)
                    table.column(self.schDOWId)
                    table.column(self.schShowId)
                }))
                print("Schedule table created successfully")
            }
            else{
                print("Schedule table creating error")
            }
            
        }catch{
            let nserror = error as NSError
            print("Create table Schedule failed. Error is: \(nserror),\(nserror.userInfo)")
        }
    }
    
    // insert record
    func insert(id: Int64,DayOfWeek: String, schdlTime: String, schdlShow: String, schDOWId: Int64, schShowId: Int64) -> Int64?
    {
        do{
            let insert = tblSchedule.insert(self.id <- id,
                                         self.DayOfWeek <- DayOfWeek,
                                         self.schdlTime <- schdlTime,
                                         self.schdlShow <- schdlShow,
                                         self.schDOWId <- schDOWId,
                                         self.schShowId <- schShowId)
            
            let insertedId = try DBHelper.shared.connection!.run(insert)
            return insertedId
        }
        catch
        {
            let nserror = error as NSError
            print("Insert into table Schedule failed. Error is: \(nserror),\(nserror.userInfo)")
            return nil
        }
    }
    
    // delete record
    func delete() -> Int?
    {
        do{
            let delete = tblSchedule.delete()
            
            let deleteId = try DBHelper.shared.connection!.run(delete)
            return deleteId
        }
        catch
        {
            let nserror = error as NSError
            print("Delete table Schedule failed. Error is: \(nserror),\(nserror.userInfo)")
            return nil
        }
    }
    
    // select all records
    func queryAll() ->AnySequence<Row>? {
        do{
            return try DBHelper.shared.connection?.prepare(self.tblSchedule)
        }catch{
            let nserror = error as NSError
            print("Select table schedule failed. Error is: \(nserror),\(nserror.userInfo)")
            return nil
        }
    }
    
    func toObject(schedule: Row) -> Schedule {
        
        let schedule_ = Schedule(schdlId: Int(schedule[self.id]),DayOfWeek: schedule[self.DayOfWeek],schdlTime: schedule[self.schdlTime],schdlShow: schedule[self.schdlShow],schDOWId: Int(schedule[self.schDOWId]),schShowId: Int(schedule[self.schShowId]))
        
       /* print("""
            Show details. id = \(schedule[self.id]),\
            DayOfWeek = \(schedule[self.DayOfWeek]),
            schdlTime = \(schedule[self.schdlTime]),
            schdlShow = \(schedule[self.schdlShow]),
            schDOWId = \(schedule[self.schDOWId]),
            schShowId = \(schedule[self.schShowId])
            """) */
        
        return schedule_!
    }
    
    func toString(schedule: Row) {
      /*  print("""
            Show details. id = \(schedule[self.id]),\
            DayOfWeek = \(schedule[self.DayOfWeek]),
            schdlTime = \(schedule[self.schdlTime]),
            schdlShow = \(schedule[self.schdlShow]),
            schDOWId = \(schedule[self.schDOWId]),
            schShowId = \(schedule[self.schShowId])
            """) */
    }
    
    func filter() -> AnySequence<Row>? {
        do{
            
            let date = Date()
            let calendar = Calendar.current
            let hour = calendar.component(.hour, from: date)
          //  let minutes = calendar.component(.minute, from: date)
            let dayOfWeek = calendar.component(.weekday, from: date)
            
            var dayOfWeekInt = Int64(dayOfWeek)
            
            if dayOfWeekInt == 1 { // .weekday starts with Sunday - 1
                dayOfWeekInt = 7
            }
            else {
                dayOfWeekInt = dayOfWeekInt - 1
            }
            
            var wakati = String(hour)
            
            // handle hour before 10 is one digit
            if wakati.count == 1 {
                wakati = "0" + wakati
            }
            
            var nyakati: [String] = []
           // var i = hour
            for i in hour...23 {
               // print(i)
                if i < 10 { // handle time before 10am
                    nyakati.append("0" + String(i))
                }
                else
                {
                    nyakati.append(String(i))
                }
            }
            
            // select * from "schedule" where ("id" = 1)
            //let filterCondition = (schDOWId == dayOfWeekInt)
            
            //select * from "schedule" where ("id" IN (1,2,3,4))
            //let filterCondition = [1,2,3,4].contains(id)
            
            //select * from "schedule" where ("name" LIKE '%Onstage')
            //let filterCondition = self.name.like('%Onstage')
            
            //select * from "schedule" where name.lowercaseString == "onstage" and id >= 3
            //let filterCondition = (id >= 3) && (name.lowercaseString == "onstage")
            
            //select * from "schedule" where ("id" == 3) OR ("id" == 3)
            // let filterCondition = (id == 3) || (id == 4)
            
            //select * from "schedule" where ("id" == 3) OR ("id" == 3)
             let filterCondition = (schDOWId == dayOfWeekInt) && (nyakati.contains(schdlTime.substring(0, length: 2)))
            //(schdlTime.substring(0, length: 2) == wakati)
            
            return try DBHelper.shared.connection?.prepare(self.tblSchedule.filter(filterCondition))
        }
        catch{
            let nserror = error as NSError
            print("Select filtered table schedule failed. Error is: \(nserror),\(nserror.userInfo)")
            return nil
        }
    }
    
}
