//
//  FollowusViewController.swift
//  Switch TV
//
//  Created by Switch TV Kenya on 03/04/2019.
//  Copyright © 2019 Switch TV. All rights reserved.
//

import UIKit
import SafariServices

class FollowusViewController: UIViewController, SFSafariViewControllerDelegate  {
    
    @IBOutlet weak var tableFollow: UITableView!
    @IBOutlet weak var backImageView: UIImageView!
    
    let titles = ["Facebook","Twitter","Instagram", "Snapchat","YouTube"]
    let icons = ["fb","twitter","instagram","snapchat","youtube"]
    let handles = ["@switchtvkenya","@switchtvkenya","@switchtvke","@switchtvkenya","@switchtvkenya"]

    override func viewDidLoad() {
        super.viewDidLoad()

        tableFollow.delegate = self
        tableFollow.dataSource = self
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        backImageView.isUserInteractionEnabled = true
        backImageView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let tappedImage = tapGestureRecognizer.view as! UIImageView
        
        // Your action
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }

}

extension FollowusViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FollowusTableViewCell") as! FollowusTableViewCell
        
        cell.setFollow(icon: UIImage(named: icons[indexPath.row])!, title: titles[indexPath.row], handle: handles[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var url: URL!
        
        if indexPath.row == 0 { // fb
            url = URL(string: "https://www.facebook.com/switchtvkenya")
        }
        else if indexPath.row == 1 { // twitter
            url = URL(string: "https://twitter.com/switchtvkenya")
        }
        else if indexPath.row == 2 { // instagram
            url = URL(string: "https://www.instagram.com/switchtvke/")
        }
        else if indexPath.row == 3 { // snapchat
          //  url = URL(string: "https://www.facebook.com/switchtvkenya")
        }
        else if indexPath.row == 4 { // youtube
            url = URL(string: "https://www.youtube.com/switchtvkenya?sub_confirmation=1")
        }
        
        if indexPath.row != 3 {
            if UIApplication.shared.canOpenURL(url!) {
                UIApplication.shared.open( url!, options: [:])
            }
            else {
                let safariVC = SFSafariViewController(url: url!)
                safariVC.delegate = self
                present(safariVC, animated: true, completion: nil)
            }
        }
    }
    
}
