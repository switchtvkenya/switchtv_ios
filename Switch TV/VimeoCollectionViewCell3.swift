//
//  VimeoCollectionViewCell3.swift
//  Switch TV
//
//  Created by Switch TV Kenya on 29/03/2019.
//  Copyright © 2019 Switch TV. All rights reserved.
//

import UIKit
import Nuke

class VimeoCollectionViewCell3: UICollectionViewCell {
    
    @IBOutlet weak var imageViewVM3: UIImageView!
    @IBOutlet weak var titleLabelVM3: UILabel!
    
    func setVimeoVideo(video: Video) {
        let url = URL(string: video.thumb)
        imageViewVM3.image = nil
        // let width = UIScreen.main.bounds.size.width
        // let height = UIScreen.main.bounds.size.height
        let request = ImageRequest(
            url: url!,
            targetSize: CGSize(width: 500, height: 300),
            //   targetSize: CGSize(width: width, height: height),
            contentMode: .aspectFit)
        Nuke.loadImage(with: request, into: imageViewVM3)
        //  Nuke.loadImage(with: url!, into: imageView)
        
        titleLabelVM3.text = video.title
        // video_ = video
        // setTapable()
        
    }
}
