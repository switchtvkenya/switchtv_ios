//
//  HomeCollectionViewCell3.swift
//  Switch TV
//
//  Created by Switch TV Kenya on 29/03/2019.
//  Copyright © 2019 Switch TV. All rights reserved.
//

import UIKit
import Nuke

class HomeCollectionViewCell3: UICollectionViewCell {
    @IBOutlet weak var imageViewNews: UIImageView!
    @IBOutlet weak var titleLabelNews: UILabel!
    @IBOutlet weak var showLabelNews: UILabel!
    
    func setHomeVideo(video: Video) {
        let url = URL(string: video.thumb)
        imageViewNews.image = nil
        // let width = UIScreen.main.bounds.size.width
        // let height = UIScreen.main.bounds.size.height
        let request = ImageRequest(
            url: url!,
            targetSize: CGSize(width: 500, height: 300),
            //   targetSize: CGSize(width: width, height: height),
            contentMode: .aspectFit)
        Nuke.loadImage(with: request, into: imageViewNews)
        //  Nuke.loadImage(with: url!, into: imageView)
        
        titleLabelNews.text = video.shortDesc
        showLabelNews.text = video.showName
        
    }
    
}
