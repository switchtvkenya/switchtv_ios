//
//  NewsViewController.swift
//  Switch TV
//
//  Created by Switch TV Kenya on 04/02/2019.
//  Copyright © 2019 Switch TV. All rights reserved.
//

import UIKit
import SQLite

class NewsViewController: UIViewController {
    
    @IBOutlet weak var newsTopMenu: NSLayoutConstraint!
    @IBOutlet weak var newsTopLabel: NSLayoutConstraint!
    @IBOutlet weak var tableViewNews: UITableView!
    @IBOutlet weak var menuImage: UIImageView!
    var videos: [Video] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
     //   print("News hapa")
        
        let modelName = UIDevice.modelName
        // print (modelName)
        
        if (modelName.contains("X") || modelName.contains("11") || modelName.contains("12"))
        {
            newsTopLabel.constant = 8
            newsTopMenu.constant = 8
        }
        
        videos = createArray()
        
        self.tableViewNews.addSubview(self.refreshControl)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        menuImage.isUserInteractionEnabled = true
        menuImage.addGestureRecognizer(tapGestureRecognizer)
}
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let tappedImage = tapGestureRecognizer.view as! UIImageView
        
        // Your action
        self.performSegue(withIdentifier: "SttSegue_News", sender: nil)
    }
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(NewsViewController.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.white
        
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        videos = createArray()
        
        self.tableViewNews.reloadData()
        refreshControl.endRefreshing()
    }
    
    func createArray() -> [Video] {
        var tempVideos: [Video] = []
        
    /*    let video1 = Video(videoId: 1,categoryId: 16,showId: 5,thumb: "https://i.ytimg.com/vi/-SO8c1ZOmE8/maxresdefault.jpg",url: "https://www.youtube.com/watch?v=-SO8c1ZOmE8", title: "Art in Kenya is as a growing industry. Art in Kenya is as a growing industry",shortDesc: "Art in Kenya is as a growing industry. Art in Kenya is as a growing industry",longDesc: "Art in Kenya is as a growing industry. Art in Kenya is as a growing industry. Art in Kenya is as a growing industry.",isAvtive: "True",isTrending: "False",parent: "1",likes: "3",views: "3",isCatchup: "False",expDate: "02 February 2021",showName: "Switch 168")
        let video2 = Video(videoId: 2,categoryId: 16,showId: 5,thumb: "https://i.ytimg.com/vi/cQLhL6tQWNQ/maxresdefault.jpg",url: "https://www.youtube.com/watch?v=cQLhL6tQWNQ", title: "Hands on the future - TVET. Hands on the future - TVET",shortDesc: "Hands on the future - TVET. Hands on the future - TVET",longDesc: "Hands on the future - TVET. Hands on the future - TVET. Hands on the future - TVET",isAvtive: "True",isTrending: "False",parent: "1",likes: "3",views: "3",isCatchup: "False",expDate: "02 February 2021",showName: "Switch 168")
        
        tempVideos.append(video1!)
        tempVideos.append(video2!) */
        
        // retrieve schedule from sqlite
        if let videoQuery: AnySequence<Row> = VideoEntity.shared.filter(type: "News"){
            for eachVideo in videoQuery{
                var video1 = VideoEntity.shared.toObject(video: eachVideo)
                tempVideos.append(video1)
            }
        }
        
        return tempVideos
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
         videos = createArray()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "YTSegue" {
            let newsVC = segue.destination as! YTViewController
            newsVC.video = sender as? Video
        }
        else if segue.identifier == "SttSegue_News" {
            let sttVC = segue.destination as! SettingsViewController
        }
    }
    
}

extension NewsViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return videos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let video = videos[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewsCell") as! NewsCell
        
        cell.setNewsVideo(video: video)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let video = videos[indexPath.item]
       // print(video.title)
        performSegue(withIdentifier: "YTSegue", sender: video)
    }
    
}
