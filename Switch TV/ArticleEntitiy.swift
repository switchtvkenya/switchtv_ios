//
//  ArticleEntitiy.swift
//  Switch TV
//
//  Created by Switch TV Kenya on 28/11/2019.
//  Copyright © 2019 Switch TV. All rights reserved.
//

import Foundation
import SQLite

class ArticleEntity {
    
    static let shared = ArticleEntity()
    
    private let tblShows = Table("Articles")
    
    private let id = Expression<Int64>("id")
    private let title = Expression<String>("title")
    private let fImage = Expression<String>("fImage")
    private let fImageCap = Expression<String>("fImageCap")
    private let fSection = Expression<String>("fSection")
    private let sImage = Expression<String>("sImage")
    private let sImageCap = Expression<String>("sImageCap")
    private let sSection = Expression<String>("sSection")
    private let videoURL = Expression<String>("videoURL")
    private let isYT = Expression<String>("isYT")
    private let isPinned = Expression<String>("isPinned")
    private let isFeatured = Expression<String>("isFeatured")
    
    
}
