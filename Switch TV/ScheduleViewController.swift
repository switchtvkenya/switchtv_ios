//
//  ScheduleViewController.swift
//  Switch TV
//
//  Created by Switch TV Kenya on 04/02/2019.
//  Copyright © 2019 Switch TV. All rights reserved.
//

import UIKit
import SQLite

class ScheduleViewController: UIViewController {
    
    @IBOutlet weak var tableViewSch: UITableView!
    
    @IBOutlet weak var menuImage: UIImageView!
    
    @IBOutlet weak var schedule: UILabel!
    
    @IBOutlet weak var schTopMenu: NSLayoutConstraint!
    @IBOutlet weak var schTopLabel: NSLayoutConstraint!
    var schedules: [Schedule] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
      //  print("schedule hapa")
        
        let modelName = UIDevice.modelName
        // print (modelName)
        
        if (modelName.contains("X") || modelName.contains("11") || modelName.contains("12"))
        {
            schTopLabel.constant = 8
            schTopMenu.constant = 8
        }
        GeneralConfigs.lockOrientation(.portrait)
        // Or to rotate and lock
        // AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        
        schedules = createArray()
        
        self.tableViewSch.addSubview(self.refreshControl)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        menuImage.isUserInteractionEnabled = true
        menuImage.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let tappedImage = tapGestureRecognizer.view as! UIImageView
        
        // Your action
        self.performSegue(withIdentifier: "SttSegue_Schedule", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SttSegue_Schedule" {
            let sttVC = segue.destination as! SettingsViewController
        }
    }
    
    func createArray() -> [Schedule] {
        var tempSchedule: [Schedule] = []
        
        /*
 {\"SchID\":\"182\",\"SchDOW\":\"Fri\",\"SchTime\":\"1630-1700\",\"SchShow\":\"CARMICHAEL SHOW\",\"SchDOWID\":\"5\",\"SchShowID\":\"60\"},{\"SchID\":\"183\",\"SchDOW\":\"Fri\",\"SchTime\":\"1700-1900\",\"SchShow\":\"SWITCHBOARD\",\"SchDOWID\":\"5\",\"SchShowID\":\"49\"},{\"SchID\":\"187\",\"SchDOW\":\"Fri\",\"SchTime\":\"1900-2000\",\"SchShow\":\"WILD FLOWER\",\"SchDOWID\":\"5\",\"SchShowID\":\"66\"},{\"SchID\":\"189\",\"SchDOW\":\"Fri\",\"SchTime\":\"2000-2030\",\"SchShow\":\"SWITCH 168\",\"SchDOWID\":\"5\",\"SchShowID\":\"5\"},{\"SchID\":\"190\",\"SchDOW\":\"Fri\",\"SchTime\":\"2030-2130\",\"SchShow\":\"HAPPY HOUR\",\"SchDOWID\":\"5\",\"SchShowID\":\"62\"},{\"SchID\":\"192\",\"SchDOW\":\"Fri\",\"SchTime\":\"2130-2230\",\"SchShow\":\"NAAGIN\",\"SchDOWID\":\"5\",\"SchShowID\":\"21\"},{\"SchID\":\"193\",\"SchDOW\":\"Fri\",\"SchTime\":\"2230-0000\",\"SchShow\":\"MOVIE: CAPTIVE\",\"SchDOWID\":\"5\",\"SchShowID\":\"52\"},{\"SchID\":\"195\",\"SchDOW\":\"Fri\",\"SchTime\":\"0000-0030\",\"SchShow\":\"SWITCH 168\",\"SchDOWID\":\"5\",\"SchShowID\":\"5\"},
 
        
        let schedule1 = Schedule(schdlId: 183,DayOfWeek: "Fri",schdlTime: "1700-1900",schdlShow: "SWITCHBOARD",schDOWId: 5,schShowId: 49)
        let schedule2 = Schedule(schdlId: 184,DayOfWeek: "Fri",schdlTime: "1900-2000",schdlShow: "WILD FLOWER",schDOWId: 5,schShowId: 66)
        let schedule3 = Schedule(schdlId: 189,DayOfWeek: "Fri",schdlTime: "2000-2030",schdlShow: "SWITCH 168",schDOWId: 1,schShowId: 5)
        let schedule4 = Schedule(schdlId: 190,DayOfWeek: "Fri",schdlTime: "2030-2130",schdlShow: "HAPPY HOUR",schDOWId: 5,schShowId: 62)
        let schedule5 = Schedule(schdlId: 192,DayOfWeek: "Fri",schdlTime: "2130-2230",schdlShow: "NAAGIN",schDOWId: 5,schShowId: 21)
        let schedule6 = Schedule(schdlId: 1,DayOfWeek: "FRI",schdlTime: "2230-0000",schdlShow: "MOVIE: CAPTIVE",schDOWId: 5,schShowId: 52)
        
        tempSchedule.append(schedule1!)
        tempSchedule.append(schedule2!)
        tempSchedule.append(schedule3!)
        tempSchedule.append(schedule4!)
        tempSchedule.append(schedule5!)
        tempSchedule.append(schedule6!) */
        
        // retrieve schedule from sqlite
        if let scheduleQuery: AnySequence<Row> = ScheduleEntity.shared.filter(){
            for eachSchedule in scheduleQuery{
                let schedule1 = ScheduleEntity.shared.toObject(schedule: eachSchedule)
                tempSchedule.append(schedule1)
            }
        }
        
        return tempSchedule
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        schedules = createArray()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        // Don't forget to reset when view is being removed
        GeneralConfigs.lockOrientation(.all)
    }
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(ScheduleViewController.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.white
        
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        schedules = createArray()
        
        self.tableViewSch.reloadData()
        refreshControl.endRefreshing()
    }
    
}

extension ScheduleViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schedules.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let schedule = schedules[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ScheduleCell") as! ScheduleCell
        
        cell.setSchedule(schedule: schedule)
        
        if indexPath.item == 0 {
            
            cell.schTitleLabel.text = "Now Playing"
            cell.schTitleLabel.textColor = UIColor.orange
        }
        
        else {
            let color = UIColor(red: 0,
                                green: 211,
                                blue: 201,
                                alpha: 1)
            cell.schTitleLabel.textColor = color
        }
        
        return cell
    }
    
}
