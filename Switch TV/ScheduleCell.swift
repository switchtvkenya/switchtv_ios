//
//  ScheduleCell.swift
//  Switch TV
//
//  Created by Switch TV Kenya on 15/02/2019.
//  Copyright © 2019 Switch TV. All rights reserved.
//

import UIKit
import Nuke

class ScheduleCell: UITableViewCell {
    
    
    @IBOutlet weak var schImageView: UIImageView!
    
    @IBOutlet weak var schTitleLabel: UILabel!
    @IBOutlet weak var schDescrLabel: UILabel!
    
    func setSchedule(schedule: Schedule) {
        let url = URL(string: "https://www.switchtv.ke/img/shows/" + String(schedule.schShowId) + "-thumb.jpg")
        schImageView.image = nil
        let request = ImageRequest(
            url: url!,
            targetSize: CGSize(width: schImageView.frame.width, height: schImageView.frame.height),
            contentMode: .aspectFill)
        schImageView.image = nil
        Nuke.loadImage(with: request, into: schImageView)
       // Nuke.loadImage(with: url!, into: schImageView)
        
        schImageView.layer.cornerRadius = schImageView.frame.height/2
        schImageView.clipsToBounds = true
        
        schTitleLabel.text = schedule.schdlTime
        schDescrLabel.text = schedule.schdlShow
        
    }
    

}
