//
//  showLandingViewController.swift
//  Switch TV
//
//  Created by Switch TV Kenya on 13/02/2019.
//  Copyright © 2019 Switch TV. All rights reserved.
//

import UIKit
import Nuke
import SQLite

class showLandingViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var ShowImageView: UIImageView!
    @IBOutlet weak var daysLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var synopsisLabel: UILabel!
    @IBOutlet weak var backImage: UIImageView!
    
    @IBOutlet weak var Row1: UICollectionView!
    @IBOutlet weak var Row2: UICollectionView!
    @IBOutlet weak var Row3: UICollectionView!
    @IBOutlet weak var showTitle: UILabel!
    
    var videos_shows: [Video] = []
    
    var show: Show?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        videos_shows = createArray()
        
        if videos_shows.count > 0 {
            Row1.delegate = self
            Row1.dataSource = self
        }
        
        if videos_shows.count > 3 {
            Row2.delegate = self
            Row2.dataSource = self
        }
        
        if videos_shows.count > 6 {
            Row3.delegate = self
            Row3.dataSource = self
        }
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        backImage.isUserInteractionEnabled = true
        backImage.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let tappedImage = tapGestureRecognizer.view as! UIImageView
        
        // Your action
       // print("Back")
        
        navigationController?.popViewController(animated: true)
        
        dismiss(animated: true, completion: nil)
    }
    
    func setUI() {
        
        ShowImageView.frame.size = CGSize(width: self.view.frame.width, height: self.view.frame.height / 4)
        
     //   let url = URL(string: "https://www.switchtv.ke/img/shows/" + String(show!.id) + "-thumb.jpg")
        let url = URL(string: "https://www.switchtv.ke/img/shows/" + String(show!.id) + ".jpg")
        ShowImageView.image = nil
       // let width = UIScreen.main.bounds.size.width
       // let height = UIScreen.main.bounds.size.height
        let request = ImageRequest(
            url: url!,
            targetSize: CGSize(width: ShowImageView.frame.width, height: 400),
          //  targetSize: CGSize(width: width, height: height),
            contentMode: .aspectFill)
        Nuke.loadImage(with: request, into: ShowImageView)
      //  Nuke.loadImage(with: url!, into: ShowImageView)
        
      /*  let daystxt = show?.showSchedule
        let timetxt = show?.showTime
        let synopsistxt = show?.description */
        
        daysLabel.text = show?.showSchedule
        timeLabel.text = show?.showTime
        synopsisLabel.text = show?.description
        showTitle.text = show?.name
    }
    
    func createArray() -> [Video] {
        
        var tempVideos: [Video] = []
        // retrieve schedule from sqlite
        if let videoQuery: AnySequence<Row> = VideoEntity.shared.getShowVideos(show: (show?.id)!){
            for eachVideo in videoQuery{
                var video1 = VideoEntity.shared.toObject(video: eachVideo)
                tempVideos.append(video1)
            }
        }
        
        return tempVideos
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "VimeoSegue_shows" {
            let vimeoVC = segue.destination as! VimeoViewController
            vimeoVC.video = sender as? Video
        }
        
        else if segue.identifier == "YTSegue_shows" {
            let newsVC = segue.destination as! YTViewController
            newsVC.video = sender as? Video
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if videos_shows.count < 3 {
            return videos_shows.count
        }
        else if videos_shows.count < 6 && collectionView == self.Row1 {
            return 3
        }
        else if videos_shows.count < 6 && collectionView == self.Row2 {
            return videos_shows.count % 3
        }
        else if videos_shows.count < 9 && collectionView == self.Row1 {
            return 3
        }
        else if videos_shows.count < 9 && collectionView == self.Row2 {
            return 3
        }
        else if videos_shows.count < 9 && collectionView == self.Row3 {
            return videos_shows.count % 3
        }
        else{
            return 3
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let count2 = 3
        let count3 = 6
        if collectionView == self.Row1 {
            let cell:ShowLandCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ShowLandCollectionViewCell", for: indexPath) as! ShowLandCollectionViewCell
            
            let video_ = videos_shows[indexPath.row]
            cell.setShowVideo(video: video_)
            
            return cell
            
        }
        else if collectionView == self.Row2 {
            let cell:ShowLandCollectionViewCell2 = collectionView.dequeueReusableCell(withReuseIdentifier: "ShowLandCollectionViewCell2", for: indexPath) as! ShowLandCollectionViewCell2
            
            let video_ = videos_shows[count2 + indexPath.row]
            cell.setShowVideo(video: video_)
            
            return cell
        }
            
        else {
            let cell:ShowLandCollectionViewCell3 = collectionView.dequeueReusableCell(withReuseIdentifier: "ShowLandCollectionViewCell3", for: indexPath) as! ShowLandCollectionViewCell3
            
            let video_ = videos_shows[count3 + indexPath.row]
            cell.setShowVideo(video: video_)
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
      //  print ("clicked YTVideo " + String(indexPath.row))
        let count2 = 3
        let count3 = 6
        
        // perform segue
        var video_ : Video?
        
        if collectionView == self.Row1 {
            video_ = videos_shows[indexPath.row]
            
        }
        else if collectionView == self.Row2 {
            video_ = videos_shows[count2 + indexPath.row]
            
        }
        else if collectionView == self.Row3{
            video_ = videos_shows[count3 + indexPath.row]
        }
        
        if video_!.url.contains("https") {
            self.performSegue(withIdentifier: "VimeoSegue_shows", sender: video_)
        }
        else {
            self.performSegue(withIdentifier: "YTSegue_shows", sender: video_)
        }
    }

}

/*
extension showLandingViewController: ShowLandingCollectionViewCellDelegate {
    func didSelectVideo(video_: Video) {
        if video_.url.contains("https") {
            self.performSegue(withIdentifier: "VimeoSegue_shows", sender: video_)
        }
        else {
            self.performSegue(withIdentifier: "YTSegue_shows", sender: video_)
        }
    }
    
    
}

extension showLandingViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ShowLandingTableViewCell", for: indexPath) as? ShowLandingTableViewCell
        cell?.registerShowCollectionView(datasource: self)
        return cell!
    }
    
}

extension showLandingViewController:UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //return videos_shows.count
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ShowLandingCollectionViewCell", for: indexPath) as? ShowLandingCollectionViewCell
        
        // cell!.frame.size = CGSize(width: 240, height: 190)
        
        // cell?.imageView.frame.size = CGSize(width: 200, height: 128)
        
        cell?.imageView.frame.size = CGSize(width: cell!.frame.size.width , height: cell!.frame.size.height - 20)
        
        /*  cell.showImageView.layer.borderWidth = 1
         cell.showImageView.layer.masksToBounds = false
         cell.showImageView.layer.borderColor = UIColor.black.cgColor
         cell.showImageView.layer.cornerRadius = cell.showImageView.frame.height/10 */
        
        //  cell!.imageView.layer.cornerRadius = cell!.imageView.frame.height/14
        //  cell!.imageView.clipsToBounds = true
        if videos_shows.count > 0 {
        let video_show = videos_shows[indexPath.row]
        
        cell?.setShowVideo(video: video_show)
        }
        
        cell?.delegate = self
        
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView,
                                 didSelectItemAt indexPath: IndexPath) {
        print("show landing")
      //  let video_show = videos_shows[indexPath.item]
        // print("show landing")
     //   performSegue(withIdentifier: "VimeoSegue_shows", sender: video_show)
    }
    
} */

