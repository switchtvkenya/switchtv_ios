//
//  Video.swift
//  Switch TV
//
//  Created by Switch TV Kenya on 07/02/2019.
//  Copyright © 2019 Switch TV. All rights reserved.
//
import Foundation
import UIKit

class Video {
    
    //MARK: Properties
    var videoId: Int
    var categoryId: Int
    var showId: Int
    var thumb: String
    var url: String
    var title: String
    var shortDesc: String
    var longDesc: String
    var isAvtive: String
    var isTrending: String
    var parent: String
    var likes: String
    var views: String
    var isCatchup: String
    var expDate: String
    var showName: String
    var latestVideo: String
   // var photo: UIImage?
    
    init?(videoId: Int, categoryId: Int, showId: Int, thumb: String, url: String, title: String, shortDesc: String, longDesc: String, isAvtive: String, isTrending: String, parent: String, likes: String, views: String, isCatchup: String, expDate: String, showName: String, latestVideo: String) {
        
        // Initialization should fail if there is no name or if the rating is negative.
     /*   if videoId == 0 || categoryId == 0 || showId == 0 || thumb.isEmpty  {
            return nil
        } */
        
        // Initialize stored properties.
        self.videoId = videoId
        self.categoryId = categoryId
        self.showId = showId
        self.thumb = thumb
        self.url = url
        self.title = title
        self.shortDesc = shortDesc
        self.longDesc = longDesc
        self.isAvtive = isAvtive
        self.showId = showId
        self.isTrending = isTrending
        self.parent = parent
        self.likes = likes
        self.views = views
        self.isCatchup = isCatchup
        self.expDate = expDate
        self.showName = showName
        self.latestVideo = latestVideo
        
    }
    
}
