//
//  LiveTVTableViewCell.swift
//  Switch TV
//
//  Created by Switch TV Kenya on 13/03/2019.
//  Copyright © 2019 Switch TV. All rights reserved.
//

import UIKit

class LiveTVTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var collectionViewLive: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func registerCollectionViewLive<DataSource:UICollectionViewDataSource>(datasource: DataSource) {
        self.collectionViewLive.dataSource = datasource
    }

}
