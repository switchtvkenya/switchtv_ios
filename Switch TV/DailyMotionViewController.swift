//
//  DailyMotionViewController.swift
//  Switch TV
//
//  Created by Switch TV Kenya on 22/07/2020.
//  Copyright © 2020 Switch TV. All rights reserved.
//

import Foundation
import UIKit
import DailymotionPlayerSDK

class DailyMotionViewController: UIViewController {

  // The player container. See setupPlayerViewController()
 // @IBOutlet private var containerView: UIView!

    @IBOutlet var containerView: UIView!
    fileprivate var initialPlayerHeight: CGFloat!
    fileprivate var isPlayerFullscreen = false
    
    private lazy var playerViewController: DMPlayerViewController = {
    // If you have an OAuth token, you can pass it to the player to hook up
    // a user's view history.
    let parameters: [String: Any] = [
      "fullscreen-action": "trigger_event", // Trigger an event when the users toggles full screen mode in the player
      "sharing-action": "trigger_event" // Trigger an event to share the video to e.g. show a UIActivityViewController
    ]
    let controller = DMPlayerViewController(parameters: parameters)
    controller.delegate = self
    return controller
  }()

  override func viewDidLoad() {
    super.viewDidLoad()
    setupPlayerViewController()
    loadVideo(withId: "x7sxle3")
  }

  // Add the player to your view. e.g. add a container on your storyboard
  // and add the player's view as subview to that
  private func setupPlayerViewController() {
    addChild(playerViewController)

    let view = playerViewController.view!
    containerView.addSubview(view)
    view.translatesAutoresizingMaskIntoConstraints = false
    // Make the player's view fit our container view
    NSLayoutConstraint.activate([
      view.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
      view.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
      view.topAnchor.constraint(equalTo: containerView.topAnchor),
      view.bottomAnchor.constraint(equalTo: containerView.bottomAnchor)
    ])
  }
    
    func loadVideo(withId id: String) {
      playerViewController.load(videoId: id)
    }

}

extension DailyMotionViewController: DMPlayerViewControllerDelegate {

  // The delegate has 4 mandatory functions

  func player(_ player: DMPlayerViewController, didReceiveEvent event: PlayerEvent) {
    // Sends player events of either .namedEvent(name: String, data: [String: String]?) or .timeEvent(name: String, time: Double)
  }

  func player(_ player: DMPlayerViewController, openUrl url: URL) {
    // Sent when a user taps on an ad that can display more information
  }

  func playerDidInitialize(_ player: DMPlayerViewController) {
    // Sent when the player has finished initializing
  }

  func player(_ player: DMPlayerViewController, didFailToInitializeWithError error: Error) {
    // Sent when the player failed to initialized
  }

}
