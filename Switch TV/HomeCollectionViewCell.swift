//
//  HomeCollectionViewCell.swift
//  Switch TV
//
//  Created by Switch TV Kenya on 19/02/2019.
//  Copyright © 2019 Switch TV. All rights reserved.
//

import UIKit
import Nuke

class HomeCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var titleLabelHome: UILabel!
    @IBOutlet weak var imageViewHome: UIImageView!
    
    func setHomeVideo(video: Video) {
        let url = URL(string: video.thumb)
        imageViewHome.image = nil
       // let width = UIScreen.main.bounds.size.width
       // let height = UIScreen.main.bounds.size.height
        let request = ImageRequest(
            url: url!,
            targetSize: CGSize(width: 500, height: 300),
         //   targetSize: CGSize(width: width, height: height),
            contentMode: .aspectFit)
        Nuke.loadImage(with: request, into: imageViewHome)
      //  Nuke.loadImage(with: url!, into: imageView)
        
        titleLabelHome.text = video.showName + ": " + video.shortDesc
        
    }
    
}
