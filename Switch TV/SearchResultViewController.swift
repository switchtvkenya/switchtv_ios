//
//  SearchResultViewController.swift
//  Switch TV
//
//  Created by Switch TV Kenya on 07/05/2019.
//  Copyright © 2019 Switch TV. All rights reserved.
//

import UIKit
import SQLite

class SearchResultViewController: UIViewController,UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var searchCollectionView: UICollectionView!
    @IBOutlet weak var backImageView: UIImageView!
    
    var videoResults: [Video] = []
    var serchPhrase: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchCollectionView.dataSource = self
        searchCollectionView.delegate = self
        
        videoResults = createArray()

        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        backImageView.isUserInteractionEnabled = true
        backImageView.addGestureRecognizer(tapGestureRecognizer)
        
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let tappedImage = tapGestureRecognizer.view as! UIImageView
        
        // Your action
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    func createArray() -> [Video]
    {
        var tempVideos: [Video] = []
        
        if let videoQuery: AnySequence<Row> = VideoEntity.shared.searchVideos(type: serchPhrase){
      //  if let videoQuery: AnySequence<Row> = VideoEntity.shared.filter(type: "News"){
            for eachVideo in videoQuery{
                var video1 = VideoEntity.shared.toObject(video: eachVideo)
                tempVideos.append(video1)
            }
        }
        
        return tempVideos
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return videoResults.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "resultCell", for: indexPath) as! SearchCollectionViewCell
        
        cell.frame.size = CGSize(width: (collectionView.frame.width - 20)/2, height: 130)
        
        let video_ = videoResults[indexPath.row]
        cell.setSearchVideo(video: video_)
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SearchSegue_Home" {
            let vimeoVC = segue.destination as! SearchResultViewController
            vimeoVC.serchPhrase = sender as? String
        }
        else if segue.identifier == "VimeoSegue_Search" {
            let vimeoVC = segue.destination as! VimeoViewController
            vimeoVC.video = sender as? Video
        }
        else if segue.identifier == "YTSegue_Search" {
            let vimeoVC = segue.destination as! YTViewController
            vimeoVC.video = sender as? Video
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
      //  self.performSegue(withIdentifier: "ShowsSegue_Home", sender: show_)
        let video_ = videoResults[indexPath.row]
        
        if video_.url.contains("https") {
            self.performSegue(withIdentifier: "VimeoSegue_Search", sender: video_)
        }
        else {
            self.performSegue(withIdentifier: "YTSegue_Search", sender: video_)
        }
    }

}
