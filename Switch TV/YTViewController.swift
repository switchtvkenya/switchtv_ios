//
//  YTViewController.swift
//  Switch TV
//
//  Created by Switch TV Kenya on 07/03/2019.
//  Copyright © 2019 Switch TV. All rights reserved.
//

import UIKit
//import YoutubeKit
import YouTubePlayer_Swift
import SQLite

class YTViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    //  private var player: YTSwiftyPlayer!

      var video: Video?
    var videos_: [Video] = []
    
    @IBOutlet weak var videoTitle: UILabel!
    @IBOutlet weak var playerView: YouTubePlayerView!
    @IBOutlet weak var backImage: UIImageView!
    //  var videoID_: String = ""

    @IBOutlet weak var YTRow1: UICollectionView!
    @IBOutlet weak var YTRow2: UICollectionView!
    @IBOutlet weak var YTRow3: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        playVideo()
        
        videos_ = createArray()
        
        if videos_.count > 0 {
            YTRow1.delegate = self
            YTRow1.dataSource = self
        }
        
        if videos_.count > 3 {
            YTRow2.delegate = self
            YTRow2.dataSource = self
        }
        
        if videos_.count > 6 {
            YTRow3.delegate = self
            YTRow3.dataSource = self
        }
        
        videoTitle.text = video?.title
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        backImage.isUserInteractionEnabled = true
        backImage.addGestureRecognizer(tapGestureRecognizer)
        
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let tappedImage = tapGestureRecognizer.view as! UIImageView
        
        // Your action
      //  print("Back")
        
        navigationController?.popViewController(animated: true)
        
        dismiss(animated: true, completion: nil)
    }
    
    func createArray() -> [Video] {
        
        var tempVideos: [Video] = []
        // retrieve schedule from sqlite
        if let videoQuery: AnySequence<Row> = VideoEntity.shared.getShowVideos(show: (video?.showId)!){
            for eachVideo in videoQuery{
                var video1 = VideoEntity.shared.toObject(video: eachVideo)
                tempVideos.append(video1)
            }
        }
        
        return tempVideos
        
    }
    
    func playVideo() {
        
        playerView.playerVars = ["playsinline": "1" as AnyObject, "controls": "1" as AnyObject, "showinfo": "1" as AnyObject]
        playerView.loadVideoID(video!.url)
        
        playerView.play()
        
       /* if playerView.ready {
            if playerView.playerState != YouTubePlayerState.Playing {
                playerView.play()
              //  playButton.setTitle("Pause", for: .normal)
            } else {
                playerView.pause()
            //playButton.setTitle("Play", for: .normal)
            }
        } */
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "VimeoSegue_YT" {
            let vimeoVC = segue.destination as! VimeoViewController
            vimeoVC.video = sender as? Video
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if videos_.count < 3 {
            return videos_.count
        }
        else{
            return 3
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let count2 = 3
        let count3 = 6
        if collectionView == self.YTRow1 {
        let cell:YTCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "YTCollectionViewCell", for: indexPath) as! YTCollectionViewCell
            
       // if indexPath.row <= 2 {
        let video_ = videos_[indexPath.row]
        cell.setYTVideo(video: video_)
      //  }
            return cell
            
        }
        else if collectionView == self.YTRow2 {
            let cell:YTCollectionViewCell2 = collectionView.dequeueReusableCell(withReuseIdentifier: "YTCollectionViewCell2", for: indexPath) as! YTCollectionViewCell2
            
            let video_ = videos_[count2  + indexPath.row]
            cell.setYTVideo(video: video_)
            
            return cell
        }
        
        else {
            let cell:YTCollectionViewCell3 = collectionView.dequeueReusableCell(withReuseIdentifier: "YTCollectionViewCell3", for: indexPath) as! YTCollectionViewCell3
            
                let video_ = videos_[count3 + indexPath.row]
                cell.setYTVideo(video: video_)
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       // print ("clicked YTVideo " + String(indexPath.row))
        let count2 = 3
        let count3 = 6
        
        var video_: Video!
        if collectionView == self.YTRow1 {
        video_ = videos_[indexPath.row]
        }
        else if collectionView == self.YTRow2 {
            video_ = videos_[count2 + indexPath.row]
        }
        else {
            video_ = videos_[count3 + indexPath.row]
        }
        
        if video_.url.contains("https") { // vimeo
          //  playerView.pause()
            performSegue(withIdentifier: "VimeoSegue_YT", sender: video_)
        }
        else { // YouTube so proceed to load video
        playerView.loadVideoID(video_.url)
        playerView.play()
        videoTitle.text = video_.title
        }
    }
}



