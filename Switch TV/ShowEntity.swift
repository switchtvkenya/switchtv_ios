//
//  ShowEntity.swift
//  Switch TV
//
//  Created by Switch TV Kenya on 11/02/2019.
//  Copyright © 2019 Switch TV. All rights reserved.
//

import Foundation
import SQLite

class ShowEntity
{
    static let shared = ShowEntity()
    
    private let tblShows = Table("Shows")
    
    private let id = Expression<Int64>("id")
    private let name = Expression<String>("name")
    private let description = Expression<String>("description")
    private let category = Expression<String>("category")
    private let hasCatchup = Expression<String>("hasCatchup")
    private let showTime = Expression<String>("showTime")
    private let showSchedule = Expression<String>("showSchedule")
    private let showPriority = Expression<Int64>("showPriority")
    
    private init()
    {
        
        // create table if doesn't exist
        do{
            if let connection = DBHelper.shared.connection {
                
               // if (connection.userVersion == 0)
                
           /*     if connection.userVersion == 0 {
                    try connection.run(tblShows.drop())
                    connection.userVersion = 1
                } */
                
                try connection.run(tblShows.create(temporary: false, ifNotExists: true, withoutRowid: false, block: { (table) in
                    table.column(self.id, primaryKey: true)
                    table.column(self.name)
                    table.column(self.description)
                    table.column(self.category)
                    table.column(self.hasCatchup)
                    table.column(self.showTime)
                    table.column(self.showSchedule)
                    table.column(self.showPriority)
                }))
                print("Shows table created successfully")
                
            }
            else{
                print("Shows table creating error")
            }
            
        }catch{
            let nserror = error as NSError
            print("Create table shows failed. Error is: \(nserror),\(nserror.userInfo)")
        }
    }
    
    // insert record
    func insert(id: Int64,name: String, description: String, category: String, hasCatchup: String, showTime: String, showSchedule: String, showPriority: Int64) -> Int64?
    {
        do{
            let insert = tblShows.insert(self.id <- id,
                                         self.name <- name,
                                         self.description <- description,
                                         self.category <- category,
                                         self.hasCatchup <- hasCatchup,
                                         self.showTime <- showTime,
                                         self.showSchedule <- showSchedule,
                                         self.showPriority <- showPriority)
            
            let insertedId = try DBHelper.shared.connection!.run(insert)
            return insertedId
        }
        catch
        {
            let nserror = error as NSError
            print("Insert into table shows failed. Error is: \(nserror),\(nserror.userInfo)")
            return nil
        }
    }
    
    // delete record
    func delete() -> Int?
    {
        do{
            let delete = tblShows.delete()
            
            let deleteId = try DBHelper.shared.connection!.run(delete)
            return deleteId
        }
        catch
        {
            let nserror = error as NSError
            print("Delete table shows failed. Error is: \(nserror),\(nserror.userInfo)")
            return nil
        }
    }
    
    // select all records
    func queryAll() ->AnySequence<Row>? {
        do{
            return try DBHelper.shared.connection?.prepare(self.tblShows.order(showPriority.asc, showPriority))
        }catch{
            let nserror = error as NSError
            print("Select table shows failed. Error is: \(nserror),\(nserror.userInfo)")
            return nil
        }
    }
    
    func filter() -> AnySequence<Row>? {
        do{
            // select * from "shows" where ("id" = 1)
            let filterCondition = (id == 1)
            
            //select * from "shows" where ("id" IN (1,2,3,4))
            //let filterCondition = [1,2,3,4].contains(id)
            
            //select * from "shows" where ("name" LIKE '%Onstage')
            //let filterCondition = self.name.like('%Onstage')
            
            //select * from "shows" where name.lowercaseString == "onstage" and id >= 3
            //let filterCondition = (id >= 3) && (name.lowercaseString == "onstage")
            
            //select * from "shows" where ("id" == 3) OR ("id" == 3)
           // let filterCondition = (id == 3) || (id == 4)
            
            return try DBHelper.shared.connection?.prepare(self.tblShows.filter(filterCondition))
        }
        catch{
            let nserror = error as NSError
            print("Select filtered table shows failed. Error is: \(nserror),\(nserror.userInfo)")
            return nil
        }
    }
    
    func toObject(shows: Row) -> Show {
        
        let show_ = Show(id: Int(shows[self.id]),name: shows[self.name],description: shows[self.description],category: shows[self.category],hasCatchup: shows[self.hasCatchup],showTime: shows[self.showTime], showSchedule: shows[self.showSchedule], showPriority: Int(shows[self.showPriority]))
        
        return show_!
    }
    
    func toString(shows: Row) {
    /*    print("""
Show details. id = \(shows[self.id]),\
            name = \(shows[self.name]),
            description = \(shows[self.description]),
            category = \(shows[self.category]),
            hasCatchup = \(shows[self.hasCatchup]),
            showTime = \(shows[self.showTime]),
            showSchedule = \(shows[self.showSchedule]))
""") */
    }
    
}

extension Connection {
    public var userVersion: Int32 {
        get { return Int32(try! scalar("PRAGMA user_version") as! Int64)}
        set { try! run("PRAGMA user_version = \(newValue)") }
    }
}
