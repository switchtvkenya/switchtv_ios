//
//  ShowLandCollectionViewCell2.swift
//  Switch TV
//
//  Created by Switch TV Kenya on 02/04/2019.
//  Copyright © 2019 Switch TV. All rights reserved.
//

import UIKit
import Nuke

class ShowLandCollectionViewCell2: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    
    func setShowVideo(video: Video) {
        let url = URL(string: video.thumb)
        imageView.image = nil
        // let width = UIScreen.main.bounds.size.width
        // let height = UIScreen.main.bounds.size.height
        let request = ImageRequest(
            url: url!,
            targetSize: CGSize(width: 500, height: 300),
            //   targetSize: CGSize(width: width, height: height),
            contentMode: .aspectFit)
        Nuke.loadImage(with: request, into: imageView)
        //  Nuke.loadImage(with: url!, into: imageView)
        
        labelTitle.text = video.title
        
    }
}
