//
//  APItoApp.swift
//  Switch TV
//
//  Created by Switch TV Kenya on 20/02/2019.
//  Copyright © 2019 Switch TV. All rights reserved.
//

import Foundation
import SQLite

class APItoApp {
    
    var tableArray = [String] ()
    
    var url_ = "http://admin.switchtv.ke.asp1-101.lan3-1.websitetestlink.com/RecruitementPortal.svc/"
    
    func putSchedule()
    {
            let url = URL(string: url_ + "Schedule")
        
            let task = URLSession.shared.dataTask(with: url!) {(data, response, error) in
                
                guard error == nil else {
                    print("returning error")
                    return
                }
                
                guard let content = data else {
                    print("not returning data")
                    return
                }
                
                guard let json = (try? JSONSerialization.jsonObject(with: content, options: JSONSerialization.ReadingOptions.mutableContainers)) as? [String: Any] else {
                    print("Not containing JSON")
                    return
                }
                
                if let ScheduleResult = json["ScheduleResult"] as? String {
                    // access individual value in dictionary
                  //  print(ScheduleResult)
                    
                    let data = ScheduleResult.data(using: .utf8)!
                    do {
                        if let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [Dictionary<String,Any>]
                        {
                          //  print(jsonArray) // use the json here
                            
                            // check whether iko ndani ndani
                            if let scheduleQuery: AnySequence<Row> = ScheduleEntity.shared.queryAll(){
                                // delete existing records first
                                let schedule2 = ScheduleEntity.shared.delete()
                               /* for eachSchedule in showQuery{
                                    ScheduleEntity.shared.toString(schedule: eachSchedule)
                                } */
                            }
                            
                                for obj in jsonArray {
                                    if let dict = obj as? NSDictionary {
                                        // Now reference the data you need using:
                                        let SchID = dict.value(forKey: "SchID") as! String
                                        let SchDOW = dict.value(forKey: "SchDOW") as! String
                                        let SchTime = dict.value(forKey: "SchTime") as! String
                                        let SchShow = dict.value(forKey: "SchShow") as! String
                                        let SchDOWID = dict.value(forKey: "SchDOWID") as! String
                                        let SchShowID = dict.value(forKey: "SchShowID") as! String
                                        
                                        let schedule = ScheduleEntity.shared.insert(id:Int64(SchID)!, DayOfWeek: SchDOW, schdlTime: SchTime, schdlShow: SchShow, schDOWId: Int64(SchDOWID)!, schShowId: Int64(SchShowID)!)
                                        
                                     /*   tempSchedule.append(Schedule(schdlId: dict.value(forKey: "SchID") as! Int, DayOfWeek: dict.value(forKey: "SchDOW") as! String, schdlTime: dict.value(forKey: "SchTime") as! String, schdlShow: dict.value(forKey: "SchShow") as! String, schDOWId: dict.value(forKey: "SchShowID") as! Int, schShowId: dict.value(forKey: "SchShowID") as! Int)!) */
                                        
                                    }
                                }
                            
                            // check whether iko ndani ndani
                           /* if let scheduleQuery: AnySequence<Row> = ScheduleEntity.shared.queryAll(){
                                for eachSchedule in scheduleQuery {
                                    ScheduleEntity.shared.toString(schedule: eachSchedule)
                                }
                            } */
                            
                        } else {
                            print("bad json")
                        }
                    } catch let error as NSError {
                        print(error)
                    }
                    
                }
                
             /*   if let array = json["ScheduleResult"] as? [String] {
                 self.tableArray = array
                 }
                 
                 print(self.tableArray) */
                
                // print(json["secure_token"])
                
                DispatchQueue.main.async {
                   //   self.tableView.reloadData()
                    
                }
                
            }
            
            task.resume()
            
            //   return token_string
            
        }
    
    func putVideos()
    {
        let url = URL(string: url_ + "Videos")
        
        let task = URLSession.shared.dataTask(with: url!) {(data, response, error) in
            
            guard error == nil else {
                print("returning error")
                return
            }
            
            guard let content = data else {
                print("not returning data")
                return
            }
            
            guard let json = (try? JSONSerialization.jsonObject(with: content, options: JSONSerialization.ReadingOptions.mutableContainers)) as? [String: Any] else {
                print("Not containing JSON")
                return
            }
            
            if let VideosResult = json["VideosResult"] as? String {
                // access individual value in dictionary
              //  print(VideosResult)
                
                let data = VideosResult.data(using: .utf8)!
                do {
                    if let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [Dictionary<String,Any>]
                    {
                      //  print(jsonArray) // use the json here
                        
                        // check whether iko ndani ndani
                        if let videosQuery: AnySequence<Row> = VideoEntity.shared.queryAll(){
                            // delete existing records first
                            let vid2 = VideoEntity.shared.delete()
                            
                        }
                        
                        for obj in jsonArray {
                            if let dict = obj as? NSDictionary {
                                // Now reference the data you need using:
                                
                                /*
                                 {\"VideoID\":\"1\",\"VideoSrcID\":\"Ai9dPgrcFpY\",\"VideoShowID\":\"3\",\"VideoURL\":\"\",\"VideoCBy\":\"1\",\"VideoCrDate\":\"1\/1\/2022 12:00:00 AM\",\"VideoIsActive\":\"True\",\"VideoIsTrending\":\"False\",\"VideoThumbNail\":\"Ai9dPgrcFpY\",\"VideoTitle\":\"E01: Rape, it can happen to you\",\"VideoShortDescr\":\"Real Talk E01: I was raped, it can happen to you\",\"VideoIsYT\":\"True\",\"VideoLikes\":\"2\",\"VideoViews\":\"33\",\"VideoIsCatchup\":\"True\",\"VideoExpDate\":\"31 December 2018\",\"ShowName\":\"Real Talk\",\"ShowEventID\":\"8446640\",\"ShowCategoryID\":\"13\",\"ShowSchedule\":\"Mondays - Tuesdays\",\"ShowTime\":\"8:30PM\",\"ShowSynopsis\":\"A show for this generation where the youth can be open about anything; learning, laughing and sharing even as they get empowered to achieve their goals and dreams.\",\"ShowParent\":\"2\"}
 
 */
                                let VideoID = dict.value(forKey: "VideoID") as! String
                                let VideoSrcID = dict.value(forKey: "VideoSrcID") as! String
                                let VideoShowID = dict.value(forKey: "VideoShowID") as! String
                                let VideoURL = dict.value(forKey: "VideoURL") as! String
                                let VideoIsActive = dict.value(forKey: "VideoIsActive") as! String
                                let VideoIsTrending = dict.value(forKey: "VideoIsTrending") as! String
                                let VideoThumbNail = dict.value(forKey: "VideoThumbNail") as! String
                                let VideoTitle = dict.value(forKey: "VideoTitle") as! String
                                let VideoShortDescr = dict.value(forKey: "VideoShortDescr") as! String
                                let VideoIsYT = dict.value(forKey: "VideoIsYT") as! String
                                let VideoLikes = dict.value(forKey: "VideoLikes") as! String
                                let VideoViews = dict.value(forKey: "VideoViews") as! String
                                let VideoIsCatchup = dict.value(forKey: "VideoIsCatchup") as! String
                                let VideoExpDate = dict.value(forKey: "VideoExpDate") as! String
                                let ShowName = dict.value(forKey: "ShowName") as! String
                                let ShowEventID = dict.value(forKey: "ShowEventID") as! String
                                let ShowCategoryID = dict.value(forKey: "ShowCategoryID") as! String
                                let ShowSchedule = dict.value(forKey: "ShowSchedule") as! String
                                let ShowTime = dict.value(forKey: "ShowTime") as! String
                                let ShowSynopsis = dict.value(forKey: "ShowSynopsis") as! String
                                let ShowParent = dict.value(forKey: "ShowParent") as! String
                                let LatestVideo = dict.value(forKey: "latestVideo") as! String
                                
                                var thumb = ""
                                var url = ""
                                
                                if VideoIsYT == "True" {
                                  //  url = "https://www.youtube.com/watch?v=" + VideoSrcID
                                    url = VideoSrcID // passing ID on the player
                                    thumb = "https://i.ytimg.com/vi/" + VideoSrcID + "/maxresdefault.jpg"
                                }
                                else if VideoIsYT == "False" {
                                   /* url = "https://livestreamapis.com/v3/accounts/27754751/events/" + ShowEventID + "/videos/" + VideoSrcID + ".m3u8"
                                    thumb = "https://img.new.livestream.com/videos/" + VideoThumbNail */
                                    url = VideoSrcID
                                    thumb = VideoThumbNail
                                }
                                
                                let video = VideoEntity.shared.insert(videoId: Int64(VideoID)!, categoryId: Int64(ShowCategoryID)!, showId: Int64(VideoShowID)!, thumb: thumb, url: url, title: VideoTitle, shortDesc: VideoShortDescr, longDesc: ShowSynopsis, isActive: VideoIsActive, isTrending: VideoIsTrending, parent: ShowParent, likes: VideoLikes, views: VideoViews, isCatchup: VideoIsCatchup, expDate: VideoExpDate, showName: ShowName, latestVideo: LatestVideo)
                                
                                /*   tempSchedule.append(Schedule(schdlId: dict.value(forKey: "SchID") as! Int, DayOfWeek: dict.value(forKey: "SchDOW") as! String, schdlTime: dict.value(forKey: "SchTime") as! String, schdlShow: dict.value(forKey: "SchShow") as! String, schDOWId: dict.value(forKey: "SchShowID") as! Int, schShowId: dict.value(forKey: "SchShowID") as! Int)!) */
                                
                            }
                        }
                        
                        // check whether iko ndani ndani
                       /* if let videoQuery: AnySequence<Row> = VideoEntity.shared.queryAll(){
                            for eachVideo in videoQuery {
                                VideoEntity.shared.toString(video: eachVideo)
                            }
                        } */
                        
                    } else {
                        print("bad json")
                    }
                } catch let error as NSError {
                    print(error)
                }
                
            }
            
            /*   if let array = json["ScheduleResult"] as? [String] {
             self.tableArray = array
             }
             
             print(self.tableArray) */
            
            // print(json["secure_token"])
            
            DispatchQueue.main.async {
                //   self.tableView.reloadData()
                
            }
            
        }
        
        task.resume()
        
        //   return token_string
        
    }
    
    func putShow()
    {
        let url = URL(string: url_ + "Shows")
        
        let task = URLSession.shared.dataTask(with: url!) {(data, response, error) in
            
            guard error == nil else {
                print("returning error")
                return
            }
            
            guard let content = data else {
                print("not returning data")
                return
            }
            
            guard let json = (try? JSONSerialization.jsonObject(with: content, options: JSONSerialization.ReadingOptions.mutableContainers)) as? [String: Any] else {
                print("Not containing JSON")
                return
            }
            
            if let ShowsResult = json["ShowsResult"] as? String {
                // access individual value in dictionary
              //  print(ShowsResult)
                
                let data = ShowsResult.data(using: .utf8)!
                do {
                    if let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [Dictionary<String,Any>]
                    {
                        //  print(jsonArray) // use the json here
                        
                        // check whether iko ndani ndani
                        if let showQuery: AnySequence<Row> = ShowEntity.shared.queryAll(){
                            // delete existing records first
                            let show2 = ShowEntity.shared.delete()
                            /* for eachSchedule in showQuery{
                             ScheduleEntity.shared.toString(schedule: eachSchedule)
                             } */
                        }
                        
                        for obj in jsonArray {
                            if let dict = obj as? NSDictionary {
                                // Now reference the data you need using:
                                
                                /*
 {\"ShowID\":\"1\",\"ShowName\":\"Onstage\",\"ShowDesc\":\"We take you inside the private lives of iconic celebrities, revealing what life really is when your very existence is in the spotlight. Heartfelt stories showcasing the price of fame.\",\"ShowHasCatchUp\":\"True\",\"ShowSchedule\":\"Fridays\",\"ShowTime\":\"8:30PM\",\"ShowCategory\":\"13\"}
 */
                                let ShowID = dict.value(forKey: "ShowID") as! String
                                let ShowName = dict.value(forKey: "ShowName") as! String
                                let ShowDesc = dict.value(forKey: "ShowDesc") as! String
                                let ShowHasCatchUp = dict.value(forKey: "ShowHasCatchUp") as! String
                                let ShowSchedule = dict.value(forKey: "ShowSchedule") as! String
                                let ShowTime = dict.value(forKey: "ShowTime") as! String
                                let ShowCategory = dict.value(forKey: "ShowCategory") as! String
                                let ShowPriority = dict.value(forKey: "ShowPriority") as! String
                                
                                let show = ShowEntity.shared.insert(id: Int64(ShowID)!, name: ShowName, description: ShowDesc, category: ShowCategory, hasCatchup: ShowHasCatchUp, showTime: ShowTime, showSchedule: ShowSchedule, showPriority: Int64(ShowPriority)!)
                                
                                /*   tempSchedule.append(Schedule(schdlId: dict.value(forKey: "SchID") as! Int, DayOfWeek: dict.value(forKey: "SchDOW") as! String, schdlTime: dict.value(forKey: "SchTime") as! String, schdlShow: dict.value(forKey: "SchShow") as! String, schDOWId: dict.value(forKey: "SchShowID") as! Int, schShowId: dict.value(forKey: "SchShowID") as! Int)!) */
                                
                            }
                        }
                        
                        // check whether iko ndani ndani
                      /*  if let showQuery: AnySequence<Row> = ShowEntity.shared.queryAll(){
                            for eachShow in showQuery {
                                ShowEntity.shared.toString(shows: eachShow)
                            }
                        } */
                        
                    } else {
                        print("bad json")
                    }
                } catch let error as NSError {
                    print(error)
                }
                
            }
            
            /*   if let array = json["ScheduleResult"] as? [String] {
             self.tableArray = array
             }
             
             print(self.tableArray) */
            
            // print(json["secure_token"])
            
            DispatchQueue.main.async {
                //   self.tableView.reloadData()
                
            }
            
        }
        
        task.resume()
        
        //   return token_string
        
    }
}
