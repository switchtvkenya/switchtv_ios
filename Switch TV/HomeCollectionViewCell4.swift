//
//  HomeCollectionViewCell4.swift
//  Switch TV
//
//  Created by Switch TV Kenya on 29/03/2019.
//  Copyright © 2019 Switch TV. All rights reserved.
//

import UIKit
import Nuke

class HomeCollectionViewCell4: UICollectionViewCell {
    
    @IBOutlet weak var imageViewEvents: UIImageView!
    @IBOutlet weak var showLabelEvents: UILabel!
    @IBOutlet weak var titleLabelEvents: UILabel!
    
    func setHomeVideo(video: Video) {
        let url = URL(string: video.thumb)
        imageViewEvents.image = nil
        // let width = UIScreen.main.bounds.size.width
        // let height = UIScreen.main.bounds.size.height
        let request = ImageRequest(
            url: url!,
            targetSize: CGSize(width: 500, height: 300),
            //   targetSize: CGSize(width: width, height: height),
            contentMode: .aspectFit)
        Nuke.loadImage(with: request, into: imageViewEvents)
        //  Nuke.loadImage(with: url!, into: imageView)
        
        titleLabelEvents.text = video.shortDesc
        showLabelEvents.text = video.showName
        
    }
    
}
