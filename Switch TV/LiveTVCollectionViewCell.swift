//
//  LiveTVCollectionViewCell.swift
//  Switch TV
//
//  Created by Switch TV Kenya on 13/03/2019.
//  Copyright © 2019 Switch TV. All rights reserved.
//

import UIKit
import Nuke

class LiveTVCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var labelDescr: UILabel!
    
    func setVideo(video: Video) {
        let url = URL(string: video.thumb)
        imageView.image = nil
        let request = ImageRequest(
            url: url!,
            targetSize: CGSize(width: 500, height: 300),
            contentMode: .aspectFill)
        Nuke.loadImage(with: request, into: imageView)
        //  Nuke.loadImage(with: url!, into: imageView)
        
        labelDescr.text = video.shortDesc
        
    }
    
}
