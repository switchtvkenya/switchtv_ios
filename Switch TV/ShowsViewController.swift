//
//  ShowsViewController.swift
//  Switch TV
//
//  Created by Switch TV Kenya on 04/02/2019.
//  Copyright © 2019 Switch TV. All rights reserved.
//

import UIKit
import Nuke
import SQLite
import GoogleMobileAds

class ShowsViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var bannerView: GADBannerView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var menuImage: UIImageView!
    
    @IBOutlet weak var showsTopLabel: NSLayoutConstraint!
    @IBOutlet weak var showsTopMenu: NSLayoutConstraint!
    @IBOutlet weak var showsLabel: UILabel!
    
    let shows = ["https://www.switchtv.ke/img/shows/1-thumb.jpg","https://www.switchtv.ke/img/shows/2-thumb.jpg","https://www.switchtv.ke/img/shows/3-thumb.jpg","https://www.switchtv.ke/img/shows/4-thumb.jpg","https://www.switchtv.ke/img/shows/5-thumb.jpg","https://www.switchtv.ke/img/shows/10-thumb.jpg","https://www.switchtv.ke/img/shows/11-thumb.jpg","https://www.switchtv.ke/img/shows/21-thumb.jpg","https://www.switchtv.ke/img/shows/25-thumb.jpg","https://www.switchtv.ke/img/shows/28-thumb.jpg"]
    
    var shows_: [Show] = []
    
    
   // let url = URL(string: "https://www.switchtv.ke/img/shows/1-thumb.jpg")
    
   // @IBOutlet weak var imageView: UIImageView!
   // let containerView = UIView(frame: CGRect(x:0,y:0,width:300,height:200))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
      //  print("Shows hapa")
        
        self.bannerView.adUnitID = "ca-app-pub-8457459103891847/3173051924"
         self.bannerView.rootViewController = self
         bannerView.load(GADRequest())
        // GADRequest().testDevices = kGADSimulatorID as? [Any]
         bannerView.delegate = self as? GADBannerViewDelegate
        
        let modelName = UIDevice.modelName
        print (modelName)
        
        if (modelName.contains("X") || modelName.contains("11") || modelName.contains("12"))
        {
            showsTopLabel.constant = 8
            showsTopMenu.constant = 8
        }
        
        collectionView.dataSource = self
        collectionView.delegate = self
        
        var layout = self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.sectionInset = UIEdgeInsets(top: 0,left: 5,bottom: 0,right: 5)
        layout.minimumInteritemSpacing = 5
        layout.itemSize = CGSize(width: (self.collectionView.frame.size.width - 20)/2, height: 220)
        
        shows_ = createArray()
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        menuImage.isUserInteractionEnabled = true
        menuImage.addGestureRecognizer(tapGestureRecognizer)
        
     /*   Nuke.loadImage(with: url!, into: imageView)
        imageView.frame.size = CGSize(width: containerView.frame.width, height: containerView.frame.height)
        imageView.layer.borderWidth = 1
        imageView.layer.masksToBounds = false
        imageView.layer.borderColor = UIColor.black.cgColor
        imageView.layer.cornerRadius = imageView.frame.height/10
      //  imageView.layer.cornerRadius = imageView.frame.height/2 circular
        imageView.clipsToBounds = true */
        
      //  Nuke.loadImage(with: url!, into: imageView)
       // Toucan(image: imageView).maskWithEllipse().image
        
      /*  let shows1 = ShowEntity.shared.insert(id:1, name:"Onstage",description: "We take you inside the private lives of iconic celebrities.", category: "3",hasCatchup: "True", showTime: "8:30PM", showSchedule: "Fridays") */
        
     /*   if let showQuery: AnySequence<Row> = ShowEntity.shared.queryAll(){
            for eachShow in showQuery{
                ShowEntity.shared.toString(shows: eachShow)
            }
        }
        
        if let showQuery: AnySequence<Row> = ShowEntity.shared.filter(){
            for eachShow in showQuery{
                ShowEntity.shared.toString(shows: eachShow)
            }
        } */
        
     /*   let shows2 = ShowEntity.shared.delete()
        
        if let showQuery1: AnySequence<Row> = ShowEntity.shared.filter(){
            for eachShow in showQuery1 {
                ShowEntity.shared.toString(shows: eachShow)
            }
        } */
}
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let tappedImage = tapGestureRecognizer.view as! UIImageView
        
        // Your action
        self.performSegue(withIdentifier: "SttSegue_Shows", sender: nil)
    }
    
    func createArray() -> [Show] {
        var tempShows: [Show] = []
        
        /*    let video1 = Video(videoId: 1,categoryId: 16,showId: 5,thumb: "https://i.ytimg.com/vi/-SO8c1ZOmE8/maxresdefault.jpg",url: "https://www.youtube.com/watch?v=-SO8c1ZOmE8", title: "Art in Kenya is as a growing industry. Art in Kenya is as a growing industry",shortDesc: "Art in Kenya is as a growing industry. Art in Kenya is as a growing industry",longDesc: "Art in Kenya is as a growing industry. Art in Kenya is as a growing industry. Art in Kenya is as a growing industry.",isAvtive: "True",isTrending: "False",parent: "1",likes: "3",views: "3",isCatchup: "False",expDate: "02 February 2021",showName: "Switch 168")
         let video2 = Video(videoId: 2,categoryId: 16,showId: 5,thumb: "https://i.ytimg.com/vi/cQLhL6tQWNQ/maxresdefault.jpg",url: "https://www.youtube.com/watch?v=cQLhL6tQWNQ", title: "Hands on the future - TVET. Hands on the future - TVET",shortDesc: "Hands on the future - TVET. Hands on the future - TVET",longDesc: "Hands on the future - TVET. Hands on the future - TVET. Hands on the future - TVET",isAvtive: "True",isTrending: "False",parent: "1",likes: "3",views: "3",isCatchup: "False",expDate: "02 February 2021",showName: "Switch 168")
         
         tempVideos.append(video1!)
         tempVideos.append(video2!) */
        
        // retrieve shows from sqlite
        if let showQuery: AnySequence<Row> = ShowEntity.shared.queryAll(){
            for eachShow in showQuery{
                let shows1 = ShowEntity.shared.toObject(shows: eachShow)
                tempShows.append(shows1)
            }
        }
        
        return tempShows
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowsSegue" {
            let showVC = segue.destination as! showLandingViewController
            showVC.show = sender as? Show
        }
        else if segue.identifier == "SttSegue_Shows" {
            let sttVC = segue.destination as! SettingsViewController
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return shows_.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! CollectionViewCell
        
        cell.frame.size = CGSize(width: (collectionView.frame.width - 20)/2, height: 220)
        
        cell.showImageView.frame.size = CGSize(width: (collectionView.frame.width - 20)/2, height: 220)
        
      /*  cell.showImageView.layer.borderWidth = 1
        cell.showImageView.layer.masksToBounds = false
        cell.showImageView.layer.borderColor = UIColor.black.cgColor
        cell.showImageView.layer.cornerRadius = cell.showImageView.frame.height/10 */
        
      //  cell.showImageView.layer.cornerRadius = cell.showImageView.frame.height/14
      //  cell.showImageView.clipsToBounds = true
        
      //  var url = URL(string: "https://www.switchtv.ke/img/shows/" + String(shows_[indexPath.item].id) + "-thumb.jpg")
        var url = URL(string: "https://www.switchtv.ke/img/showthumbnails/" + String(shows_[indexPath.item].id) + "-thumb.png")
        let request = ImageRequest(
            url: url!,
            targetSize: CGSize(width: cell.frame.size.width, height: 220),
            contentMode: .aspectFill)
        cell.showImageView.image = nil
         Nuke.loadImage(with: request, into: cell.showImageView)
      //  Nuke.loadImage(with: url!, into: cell.showImageView)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        print(indexPath.item)
        let show = shows_[indexPath.item]
        performSegue(withIdentifier: "ShowsSegue", sender: show)
        
      /*  let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "showLandingViewController") as! showLandingViewController
        self.present(newViewController, animated: true, completion: nil)
        
        let vc = showLandingViewController(nibName: "showLandingViewController", bundle: nil)
        
        vc.text_ = shows_[indexPath.item].name
        
        navigationController?.pushViewController(vc, animated: true)  */
        
      /*  cell?.layer.borderColor = UIColor.gray.cgColor
        cell?.layer.borderWidth = 2 */
        
    }
    
}
