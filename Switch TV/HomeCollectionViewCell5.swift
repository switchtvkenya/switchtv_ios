//
//  HomeCollectionViewCell5.swift
//  Switch TV
//
//  Created by Switch TV Kenya on 29/03/2019.
//  Copyright © 2019 Switch TV. All rights reserved.
//

import UIKit
import Nuke

class HomeCollectionViewCell5: UICollectionViewCell {
    
    @IBOutlet weak var imageViewShows: UIImageView!
    
    
    func setShows(shows: Show)
    {
      //  var url = URL(string: "https://www.switchtv.ke/img/shows/" + String(shows.id) + "-thumb.jpg")
        var url = URL(string: "https://www.switchtv.ke/img/showthumbnails/" + String(shows.id) + "-thumb.png")
        let request = ImageRequest(
            url: url!,
            targetSize: CGSize(width: 370, height: 555),
            contentMode: .aspectFill)
        imageViewShows.image = nil
        Nuke.loadImage(with: request, into: imageViewShows)
        //  Nuke.loadImage(with: url!, into: cell.showImageView)
        
      //  titleLabelHome.text = video.shortDesc
    }
    
}
