//
//  ShowLandingTableViewCell.swift
//  Switch TV
//
//  Created by Switch TV Kenya on 14/03/2019.
//  Copyright © 2019 Switch TV. All rights reserved.
//

import UIKit

class ShowLandingTableViewCell: UITableViewCell {

    @IBOutlet weak var showLandingcollectionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func registerShowCollectionView<DataSource:UICollectionViewDataSource>(datasource: DataSource) {
        self.showLandingcollectionView.dataSource = datasource
    }

}
