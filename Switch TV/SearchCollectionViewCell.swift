//
//  SearchCollectionViewCell.swift
//  Switch TV
//
//  Created by Switch TV Kenya on 07/05/2019.
//  Copyright © 2019 Switch TV. All rights reserved.
//

import UIKit
import Nuke

class SearchCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var videoImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    func setSearchVideo(video: Video) {
        let url = URL(string: video.thumb)
        videoImageView.image = nil
         let width = UIScreen.main.bounds.size.width
        // let height = UIScreen.main.bounds.size.height
        let request = ImageRequest(
            url: url!,
            targetSize: CGSize(width: width, height: 115),
            contentMode: .aspectFit)
        Nuke.loadImage(with: request, into: videoImageView)
        
        titleLabel.text = video.shortDesc
        
    }
    
}
