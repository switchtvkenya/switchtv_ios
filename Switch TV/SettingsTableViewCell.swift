//
//  SettingsTableViewCell.swift
//  Switch TV
//
//  Created by Switch TV Kenya on 03/04/2019.
//  Copyright © 2019 Switch TV. All rights reserved.
//

import UIKit

class SettingsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var imageViewClick: UIImageView!
    @IBOutlet weak var imageViewStt: UIImageView!
    @IBOutlet weak var titleLabelStt: UILabel!
    
    func setStt(icon: UIImage, title: String, iconNext: UIImage, subtitle: String) {
        
        imageViewStt.image = icon
        titleLabelStt.text = title
        imageViewClick.image = iconNext
        subtitleLabel.text = subtitle
    }

}
