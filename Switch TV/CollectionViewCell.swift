//
//  CollectionViewCell.swift
//  Switch TV
//
//  Created by Switch TV Kenya on 13/02/2019.
//  Copyright © 2019 Switch TV. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var showImageView: UIImageView!
    
}
