//
//  LiveTVViewController.swift
//  Switch TV
//
//  Created by Switch TV on 03/12/2018.
//  Copyright © 2018 Switch TV. All rights reserved.
//

import UIKit
//import YouTubePlayer
//import YoutubeKit
import VersaPlayer
import SQLite

class LiveTVViewController: UIViewController  {
    //@IBOutlet var videoPlayer: YouTubePlayerView!
    @IBOutlet weak var view_yt: UIView!
    
    @IBOutlet weak var playerView: VersaPlayerView!
    
    @IBOutlet weak var menuImage: UIImageView!
    
    var tableArray = [String] ()
    
    var videos: [Video] = []
    
 //   private var player: YTSwiftyPlayer!

    @IBOutlet weak var liveTopMenu: NSLayoutConstraint!
    @IBOutlet weak var liveTopLabel: NSLayoutConstraint!
    @IBOutlet var controls: VersaPlayerControls!
    //  @IBOutlet var yt: YouTubePlayerView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        //videoPlayer.loadVideoID("nfWlot6h_JM")
        
        let modelName = UIDevice.modelName
        print (modelName)
        
        if (modelName.contains("X") || modelName.contains("11") || modelName.contains("12"))
        {
            liveTopLabel.constant = 8
            liveTopMenu.constant = 8
        }
        
        playerView.layer.backgroundColor = UIColor.black.cgColor
        playerView.use(controls: controls)
        
     //   if let url = URL.init(string: "https://bitdash-a.akamaihd.net/content/sintel/hls/playlist.m3u8") {
        parseJSON()
        
        
        // Create a new player
     /*   player = YTSwiftyPlayer(
            frame: CGRect(x: 0, y: 0, width: 640, height: 480),
            playerVars: [.videoID("7gNgWuk_Lpc")])
        
        // Enable auto playback when video is loaded
        player.autoplay = true
        
        // Set player view.
        view = player
        
        // Set delegate for detect callback information from the player.
        player.delegate = self as? YTSwiftyPlayerDelegate
        
        // Load the video.
        player.loadPlayer()
        
        parseJSON() */
        
        videos = trending()
        
    /*    if videos.count > 0 {
            Row1.delegate = self
            Row1.dataSource = self
        }
        
        if videos.count > 2 {
            Row2.delegate = self
            Row2.dataSource = self
        } */
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        menuImage.isUserInteractionEnabled = true
        menuImage.addGestureRecognizer(tapGestureRecognizer)
        
        playerView.frame.size = CGSize(width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height - 100)
        
    /*    if playerView.isFullscreenModeEnabled {
            print("full screen")
        }
        else{
            playerView.frame.size = CGSize(width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height * 0.4)
        } */
        
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let tappedImage = tapGestureRecognizer.view as! UIImageView
        
        // Your action
        self.performSegue(withIdentifier: "SttSegue_Live", sender: nil)
    }

    func parseJSON(){
       // let url = URL(string: "https://api.myjson.com/bins/vi56v")
        let url = URL(string: "https://tungana.tech/switchtv/token.php")
        var token_string = ""
        var token_ = ""
        var times_tamp_ = ""
        var client_id_ = ""
        let task = URLSession.shared.dataTask(with: url!) {(data, response, error) in
            
            guard error == nil else {
                print("returning error")
                return
            }
            
            guard let content = data else {
                print("not returning data")
                return
            }
            
            guard let json = (try? JSONSerialization.jsonObject(with: content, options: JSONSerialization.ReadingOptions.mutableContainers)) as? [String: Any] else {
                print("Not containing JSON")
                return
            }
            
            if let token = json["secure_token"] as? String {
                // access individual value in dictionary
                print(token)
                token_ = token
            }
            
            if let times_tamp = json["timestamp"] as? Int {
                // access individual value in dictionary
                print(times_tamp)
                times_tamp_ = String(times_tamp)
            }
            
            // client_id":8604
            
            if let client_id = json["client_id"] as? Int {
                // access individual value in dictionary
                print(client_id)
                client_id_ = String(client_id)
            }
            
            token_string = "?timestamp=" + times_tamp_ + "&clientId=" + client_id_ + "&token=" + token_
            
        /*    if let array = json["secure_token"] as? [String] {
                self.tableArray = array
            }
            
            print(self.tableArray) */
            
           // print(json["secure_token"])
            
            DispatchQueue.main.async {
              //  self.tableView.reloadData()
                
                if let url = URL.init(string: "https://livestreamapis.com/v3/accounts/27754751/events/8377002/master.m3u8" + token_string) {
                    let item = VersaPlayerItem(url: url)
                    self.playerView.set(item: item)
                }
            }
            
        }
        
        task.resume()
        
     //   return token_string
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
      //  print("pause hapa")
        self.playerView.pause()
    }
    
  /*  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "VimeoSegue_Live" {
            let vimeoVC = segue.destination as! VimeoViewController
            vimeoVC.video = sender as? Video
        }
        else if segue.identifier == "YTSegue_Live" {
            let vimeoVC = segue.destination as! YTViewController
            vimeoVC.video = sender as? Video
        }
        else if segue.identifier == "SttSegue_Live" {
            let sttVC = segue.destination as! SettingsViewController
        }
    } */
    
    func trending() -> [Video] {
        
        var tempVideos: [Video] = []
        // retrieve schedule from sqlite
        if let videoQuery: AnySequence<Row> = VideoEntity.shared.filter(type: "Trending") {
            for eachVideo in videoQuery{
                var video1 = VideoEntity.shared.toObject(video: eachVideo)
                tempVideos.append(video1)
            }
        }
        
        return tempVideos
        
    }
    
  /*  func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if videos.count < 2 {
            return videos.count
        }
        else{
            return 2
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let count2 = 2
        
        if collectionView == self.Row1 {
            let cell:LiveCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "LiveCollectionViewCell", for: indexPath) as! LiveCollectionViewCell
            
            let video_ = videos[indexPath.row]
            cell.setTrendingVideo(video: video_)
            
            return cell

        }
            
        else {
            let cell:LiveCollectionViewCell2 = collectionView.dequeueReusableCell(withReuseIdentifier: "LiveCollectionViewCell2", for: indexPath) as! LiveCollectionViewCell2
            
            let video_ = videos[count2 + indexPath.row]
            cell.setTrendingVideo(video: video_)
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
      //  print ("clicked LiveTV " + String(indexPath.row))
        let count2 = 2
        if collectionView == self.Row1 {
           let video = videos[indexPath.row]
            if video.url.contains("https") {// vimeo
            self.performSegue(withIdentifier: "VimeoSegue_Live", sender: video)
            }
            else {
                self.performSegue(withIdentifier: "YTSegue_Live", sender: video)
            }
        }
            
        else { // Row 2
            let video = videos[count2 + indexPath.row]
            if video.url.contains("https") {// vimeo
                self.performSegue(withIdentifier: "VimeoSegue_Live", sender: video)
            }
            else {
                self.performSegue(withIdentifier: "YTSegue_Live", sender: video)
            }
        }
    } */
}
