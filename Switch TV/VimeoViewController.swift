//
//  VimeoViewController.swift
//  Switch TV
//
//  Created by Switch TV Kenya on 15/03/2019.
//  Copyright © 2019 Switch TV. All rights reserved.
//

import UIKit
import VersaPlayer
import SQLite
import BMPlayer

class VimeoViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var shkilia: UILabel!
    @IBOutlet var parentView: UIView!
    @IBOutlet weak var nav1: UINavigationBar!
    @IBOutlet weak var nav2: UINavigationBar!
    @IBOutlet weak var videoTitleLabel: UILabel!
   // @IBOutlet weak var player: VersaPlayerView!
    @IBOutlet weak var player: BMCustomPlayer!
    @IBOutlet var controls: VersaPlayerControls!
    @IBOutlet weak var backImage: UIImageView!
    
    @IBOutlet weak var labelEpsiodes: UILabel!
    @IBOutlet weak var VMRow1: UICollectionView!
    @IBOutlet weak var VMRow2: UICollectionView!
    @IBOutlet weak var VMRow3: UICollectionView!
    @IBOutlet weak var scroll: UIScrollView!
    
    var video: Video!
    var video_: Video!
    var videos_: [Video] = []
    
    var margins: UILayoutGuide!
    var margins2: UILayoutGuide!
    
    var bottomConstraint:NSLayoutConstraint!
    var bottomConstraint2:NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
     //   player.layer.backgroundColor = UIColor.black.cgColor
      //  player.use(controls: controls)
        
       /* let modelName = UIDevice.modelName
        
        if (modelName.contains("X") || modelName.contains("11") || modelName.contains("12"))
        {
            
        } */
        
        player.backBlock = { [unowned self] (isFullScreen) in
            if isFullScreen == true {
                return
            }
            let _ = self.navigationController?.popViewController(animated: true)
        }
        
        margins = shkilia.layoutMarginsGuide
        margins2 = parentView.layoutMarginsGuide
        
        bottomConstraint = player.topAnchor.constraint(equalTo: margins.bottomAnchor)
       // bottomConstraint2 = player.topAnchor.constraint(equalTo: margins2.bottomAnchor)
        
       // player.topAnchor.constraint(equalTo: margins.bottomAnchor).isActive = true
        
        bottomConstraint.isActive = true
        
        videos_ = createArray()
        
        parseJSON(video_passed: video)
        setUI()
        
        if videos_.count > 0 {
            VMRow1.delegate = self
            VMRow1.dataSource = self
        }
        
        if videos_.count > 3 {
            VMRow2.delegate = self
            VMRow2.dataSource = self
        }
        
        if videos_.count > 6 {
            VMRow3.delegate = self
            VMRow3.dataSource = self
        }
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        backImage.isUserInteractionEnabled = true
        backImage.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let tappedImage = tapGestureRecognizer.view as! UIImageView
        
        // Your action
        // print("Back")
        
        navigationController?.popViewController(animated: true)
        
        dismiss(animated: true, completion: nil)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        // Get the superview's layout
     //   let margins = parentView.layoutMarginsGuide
     //   let margins2 = shkilia.layoutMarginsGuide
        
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            // Pin the leading edge of myView to the margin's leading edge
        /*    player.leadingAnchor.constraint(equalTo: margins.leadingAnchor).isActive = true
            
            // Pin the trailing edge of myView to the margin's trailing edge
            player.trailingAnchor.constraint(equalTo: margins.trailingAnchor).isActive = true
            
            player.topAnchor.constraint(equalTo: margins.topAnchor).isActive = true
            
            player.bottomAnchor.constraint(equalTo: margins.bottomAnchor).isActive = true */
            
            // Give myView a 1:2 aspect ratio
           // player.heightAnchor.constraint(equalTo: player.widthAnchor, multiplier: 2.0).isActive = true
            
          //  player.topAnchor.constraint(equalTo: margins2.topAnchor).isActive = true
            bottomConstraint.isActive = false
         //   bottomConstraint2.isActive = false
            
            nav1.isHidden = true
            nav2.isHidden = true
            backImage.isHidden = true
            scroll.isHidden = true
            videoTitleLabel.isHidden = true
            labelEpsiodes.isHidden = true
            
        } else {
            print("Portrait")
            
        //    player.removeConstraint(player.topAnchor.constraint(equalTo: margins.topAnchor))
            
         //   player.topAnchor.constraint(equalTo: margins.topAnchor).isActive = false
          //  player.topAnchor.constraint(equalTo: margins2.topAnchor).isActive = true
         //   player.topAnchor.constraint(equalTo: margins2.topAnchor).isActive = false
         //   player.topAnchor.constraint(equalTo: margins.topAnchor).isActive = true
            
            bottomConstraint.isActive = true
          //  bottomConstraint2.isActive = true
            
            scroll.isHidden = false
            videoTitleLabel.isHidden = false
            labelEpsiodes.isHidden = false
            nav1.isHidden = false
            nav2.isHidden = false
            backImage.isHidden = false
            
            // square.widthAnchor.constraint(equalToConstant: 64)
          /*  player.leadingAnchor.constraint(equalTo: margins.leadingAnchor).isActive = false
            
            // Pin the trailing edge of myView to the margin's trailing edge
            player.trailingAnchor.constraint(equalTo: margins.trailingAnchor).isActive = false
            
            player.topAnchor.constraint(equalTo: margins.topAnchor).isActive = false
            player.bottomAnchor.constraint(equalTo: margins.bottomAnchor).isActive = false
            
            player.frame.size = CGSize(width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height / 3) */
        }
    }
    
    func setUI() {
        videoTitleLabel.text = video?.title
    }
    
    func createArray() -> [Video] {
        
        var tempVideos: [Video] = []
        // retrieve schedule from sqlite
        if let videoQuery: AnySequence<Row> = VideoEntity.shared.getShowVideos(show: (video?.showId)!){
            for eachVideo in videoQuery{
                var video1 = VideoEntity.shared.toObject(video: eachVideo)
                tempVideos.append(video1)
            }
        }
        
        return tempVideos
        
    }
    
    func parseJSON(video_passed: Video){
        // let url = URL(string: "https://api.myjson.com/bins/vi56v")
        let url = URL(string: "https://tungana.tech/switchtv/token.php")
        var token_string = ""
        var token_ = ""
        var times_tamp_ = ""
        var client_id_ = ""
        let task = URLSession.shared.dataTask(with: url!) {(data, response, error) in
            
            guard error == nil else {
                print("returning error")
                return
            }
            
            guard let content = data else {
                print("not returning data")
                return
            }
            
            guard let json = (try? JSONSerialization.jsonObject(with: content, options: JSONSerialization.ReadingOptions.mutableContainers)) as? [String: Any] else {
                print("Not containing JSON")
                return
            }
            
            if let token = json["secure_token"] as? String {
                // access individual value in dictionary
                print(token)
                token_ = token
            }
            
            if let times_tamp = json["timestamp"] as? Int {
                // access individual value in dictionary
                print(times_tamp)
                times_tamp_ = String(times_tamp)
            }
            
            // client_id":8604
            
            if let client_id = json["client_id"] as? Int {
                // access individual value in dictionary
                print(client_id)
                client_id_ = String(client_id)
            }
            
            token_string = "?timestamp=" + times_tamp_ + "&clientId=" + client_id_ + "&token=" + token_
            
            /*    if let array = json["secure_token"] as? [String] {
             self.tableArray = array
             }
             
             print(self.tableArray) */
            
            // print(json["secure_token"])
            
            DispatchQueue.main.async {
                //  self.tableView.reloadData()
                
                if let url = URL.init(string: video_passed.url + token_string) {
                //    let item = VersaPlayerItem(url: url)
                    //self.player.set(item: item)
                    
                    var vidTitle = video_passed.title
                    if vidTitle.count > 35 {
                        vidTitle = vidTitle.prefix(35) + "..."
                    }
                    
                    let asset = BMPlayerResource(url: url,
                                                 name: vidTitle,
                                                 cover: nil,
                                                 subtitle: nil)
                    self.player.setVideo(resource: asset)
                }
            }
            
        }
        
        task.resume()
        
        //   return token_string
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "YTSegue_Vimeo" {
            let newsVC = segue.destination as! YTViewController
            newsVC.video = sender as? Video
        }
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
      //  print("imekam")
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if videos_.count < 3 {
            return videos_.count
        }
        else if videos_.count < 6 && collectionView == self.VMRow1 {
            return 3
        }
        else if videos_.count < 6 && collectionView == self.VMRow2 {
            return videos_.count % 3
        }
        else if videos_.count < 9 && collectionView == self.VMRow1 {
            return 3
        }
        else if videos_.count < 9 && collectionView == self.VMRow2 {
            return 3
        }
        else if videos_.count < 9 && collectionView == self.VMRow3 {
            return videos_.count % 3
        }
        else{
            return 3
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let count2 = 3
        let count3 = 6
        if collectionView == self.VMRow1 {
            let cell:VimeoCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "VimeoCollectionViewCell", for: indexPath) as! VimeoCollectionViewCell
            
            // if indexPath.row <= 2 {
            let video_ = videos_[indexPath.row]
            cell.setVimeoVideo(video: video_)
            //  }
            return cell
            
        }
        else if collectionView == self.VMRow2 {
            let cell:VimeoCollectionViewCell2 = collectionView.dequeueReusableCell(withReuseIdentifier: "VimeoCollectionViewCell2", for: indexPath) as! VimeoCollectionViewCell2
            
            let video_ = videos_[count2 + indexPath.row]
            cell.setVimeoVideo(video: video_)
            
            return cell
        }
            
        else {
            let cell:VimeoCollectionViewCell3 = collectionView.dequeueReusableCell(withReuseIdentifier: "VimeoCollectionViewCell3", for: indexPath) as! VimeoCollectionViewCell3
            
            let video_ = videos_[count3 + indexPath.row]
            cell.setVimeoVideo(video: video_)
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
      //  print ("clicked VMVideo " + String(indexPath.row))
        let count2 = 3
        let count3 = 6
       // var video: Video?
        if collectionView == self.VMRow1 {
            video_ = videos_[indexPath.row]
        }
        else if collectionView == self.VMRow2 {
            video_ = videos_[count2 + indexPath.row]
        }
            
        else if collectionView == self.VMRow3 {
            video_ = videos_[count3 + indexPath.row]
        }
        
        if video_!.url.contains("https") {
            videoTitleLabel.text = video_!.title
            parseJSON(video_passed: video_)
        }
        else { // YT videos
            // Load YT view
            performSegue(withIdentifier: "YTSegue_Vimeo", sender: video_)
            
            // close Vimeo View
          //  self.navigationController?.popViewController(animated: true)
         //   self.dismiss(animated: true, completion: nil)
        }
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        //  print("pause hapa")
    /*    if !video_!.url.contains("https") {
        
        } */
        
        self.player.pause()
    }

}
