//
//  DBHelper.swift
//  Switch TV
//
//  Created by Switch TV Kenya on 11/02/2019.
//  Copyright © 2019 Switch TV. All rights reserved.
//

import Foundation
import SQLite

class DBHelper
{
    static let shared = DBHelper()
    public let connection: Connection?
    public let databaseFileName = "SwitchTV.sqlite3"
    private init()
    {
        let dbPath = NSSearchPathForDirectoriesInDomains (.documentDirectory, .userDomainMask,true).first as String!
        do{
            connection = try Connection("\(dbPath!)/(databaseFileName)")
        }catch{
            connection = nil
            let nserror = error as NSError
            print("Cannot connect to database. Error is: \(nserror),\(nserror.userInfo)")
        }
    }
}
