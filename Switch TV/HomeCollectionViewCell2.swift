//
//  HomeCollectionViewCell2.swift
//  Switch TV
//
//  Created by Switch TV Kenya on 29/03/2019.
//  Copyright © 2019 Switch TV. All rights reserved.
//

import UIKit
import Nuke

class HomeCollectionViewCell2: UICollectionViewCell {
    
    @IBOutlet weak var imageViewCatchup: UIImageView!
    @IBOutlet weak var showLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    func setHomeVideo(video: Video) {
        let url = URL(string: video.thumb)
        imageViewCatchup.image = nil
        // let width = UIScreen.main.bounds.size.width
        // let height = UIScreen.main.bounds.size.height
        let request = ImageRequest(
            url: url!,
            targetSize: CGSize(width: 500, height: 300),
            //   targetSize: CGSize(width: width, height: height),
            contentMode: .aspectFit)
        Nuke.loadImage(with: request, into: imageViewCatchup)
        //  Nuke.loadImage(with: url!, into: imageView)
        
        titleLabel.text = video.shortDesc
        showLabel.text = video.showName
        
    }
    
}
