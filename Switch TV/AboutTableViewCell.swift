//
//  AboutTableViewCell.swift
//  Switch TV
//
//  Created by Switch TV Kenya on 03/04/2019.
//  Copyright © 2019 Switch TV. All rights reserved.
//

import UIKit

class AboutTableViewCell: UITableViewCell {

    @IBOutlet weak var body: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    func setAbout(title: String, descr: String) {
        
        titleLabel.text = title
        body.text = descr
        
    }

}
