//
//  VideoEntity.swift
//  Switch TV
//
//  Created by Switch TV Kenya on 21/02/2019.
//  Copyright © 2019 Switch TV. All rights reserved.
//

import Foundation
import SQLite

class VideoEntity
{
    static let shared = VideoEntity()
    
    private let tblVideos = Table("Videos")
    
    private let videoId = Expression<Int64>("videoId")
    private let categoryId = Expression<Int64>("categoryId")
    private let showId = Expression<Int64>("showId")
    private let thumb = Expression<String>("thumb")
    private let url = Expression<String>("url")
    private let title = Expression<String>("title")
    private let shortDesc = Expression<String>("shortDesc")
    private let longDesc = Expression<String>("longDesc")
    private let isActive = Expression<String>("isActive")
    private let isTrending = Expression<String>("isTrending")
    private let parent = Expression<String>("parent")
    private let likes = Expression<String>("likes")
    private let views = Expression<String>("views")
    private let isCatchup = Expression<String>("isCatchup")
    private let expDate = Expression<String>("expDate")
    private let showName = Expression<String>("showName")
    private let latestVideo = Expression<String>("latestVideo")
    
    private init()
    {
        // create table if doesn't exist
        do{
            if let connection = DBHelper.shared.connection {
              /*  if connection.userVersion == 1 {
                    try connection.run(tblVideos.drop())
                    connection.userVersion = 2
                } */
                
                try connection.run(tblVideos.create(temporary: false, ifNotExists: true, withoutRowid: false, block: { (table) in
                    table.column(self.videoId, primaryKey: true)
                    table.column(self.categoryId)
                    table.column(self.showId)
                    table.column(self.thumb)
                    table.column(self.url)
                    table.column(self.title)
                    table.column(self.shortDesc)
                    table.column(self.longDesc)
                    table.column(self.isActive)
                    table.column(self.isTrending)
                    table.column(self.parent)
                    table.column(self.likes)
                    table.column(self.views)
                    table.column(self.isCatchup)
                    table.column(self.expDate)
                    table.column(self.showName)
                    table.column(self.latestVideo)
                }))
                print("Videos table created successfully")
            }
            else{
                print("Videos table creating error")
            }
            
        }catch{
            let nserror = error as NSError
            print("Create table Videos failed. Error is: \(nserror),\(nserror.userInfo)")
        }
    }
    
    // insert record
    func insert(videoId: Int64, categoryId: Int64, showId: Int64, thumb: String, url: String, title: String, shortDesc: String, longDesc: String, isActive: String, isTrending: String, parent: String, likes: String, views: String, isCatchup: String, expDate: String, showName: String, latestVideo: String) -> Int64?
    {
        do{
            let insert = tblVideos.insert(self.videoId <- videoId,
                                         self.categoryId <- categoryId,
                                         self.showId <- showId,
                                         self.thumb <- thumb,
                                         self.url <- url,
                                         self.title <- title,
                                         self.shortDesc <- shortDesc,
                                         self.longDesc <- longDesc,
                                         self.isActive <- isActive,
                                         self.isTrending <- isTrending,
                                         self.parent <- parent,
                                         self.likes <- likes,
                                         self.views <- views,
                                         self.isCatchup <- isCatchup,
                                         self.expDate <- expDate,
                                         self.showName <- showName,
                                         self.latestVideo <- latestVideo)
            
            let insertedId = try DBHelper.shared.connection!.run(insert)
            return insertedId
        }
        catch
        {
            let nserror = error as NSError
            print("Insert into table Videos failed. Error is: \(nserror),\(nserror.userInfo)")
            return nil
        }
    }
    
    // delete record
    func delete() -> Int?
    {
        do{
            let delete = tblVideos.delete()
            
            let deleteId = try DBHelper.shared.connection!.run(delete)
            return deleteId
        }
        catch
        {
            let nserror = error as NSError
            print("Delete table Videos failed. Error is: \(nserror),\(nserror.userInfo)")
            return nil
        }
    }
    
    // select all records
    func queryAll() ->AnySequence<Row>? {
        do{
            return try DBHelper.shared.connection?.prepare(self.tblVideos)
        }catch{
            let nserror = error as NSError
            print("Select table Videos failed. Error is: \(nserror),\(nserror.userInfo)")
            return nil
        }
    }
    
    func getCatchup() -> AnySequence<Row>? {
        do{
            // select * from "shows" where ("id" = 1)
            // let filterCondition = (videoId == 1)
            
            //select * from "shows" where ("id" IN (1,2,3,4))
            //let filterCondition = [1,2,3,4].contains(id)
            
            //select * from "shows" where ("name" LIKE '%Onstage')
            //let filterCondition = self.name.like('%Onstage')
            
            //select * from "shows" where name.lowercaseString == "onstage" and id >= 3
            //let filterCondition = (id >= 3) && (name.lowercaseString == "onstage")
            
            //select * from "shows" where ("id" == 3) OR ("id" == 3)
            
         /*   let dateFormate = DateFormatter()
            dateFormate.dateFormat = "dd MMMM yyyy"
            let date = NSDate()
            let stringOfDate = dateFormate.string(from: date as Date) */
            
          //  print (expDate)
        //    print (stringOfDate)
            
            let filterCondition = (isActive == "True") && (latestVideo == "True") && !(showId == 5) && !(showId == 89) && !(showId == 90)
            
                // table.filter(contains(table.select(max(age)).group(id), age)).order(id)
            
            return try DBHelper.shared.connection?.prepare(self.tblVideos.filter(filterCondition).group(showId).order(videoId.desc, videoId))
        }
        catch{
            let nserror = error as NSError
            print("Select filtered table Videos failed. Error is: \(nserror),\(nserror.userInfo)")
            return nil
        }
    }
    
    func getShowVideos(show: Int) -> AnySequence<Row>? {
        do{
            // select * from "shows" where ("id" = 1)
            // let filterCondition = (videoId == 1)
            
            //select * from "shows" where ("id" IN (1,2,3,4))
            //let filterCondition = [1,2,3,4].contains(id)
            
            //select * from "shows" where ("name" LIKE '%Onstage')
            //let filterCondition = self.name.like('%Onstage')
            
            //select * from "shows" where name.lowercaseString == "onstage" and id >= 3
            //let filterCondition = (id >= 3) && (name.lowercaseString == "onstage")
            
            //select * from "shows" where ("id" == 3) OR ("id" == 3)
            
            let dateFormate = DateFormatter()
            dateFormate.dateFormat = "dd MMMM yyyy"
            let date = NSDate()
            let stringOfDate = dateFormate.string(from: date as Date)
         //   print(stringOfDate)
            
            let filterCondition = (isActive == "True") && (showId == Int64(show))
            
            return try DBHelper.shared.connection?.prepare(self.tblVideos.filter(filterCondition).order(videoId.desc, videoId))
        }
        catch{
            let nserror = error as NSError
            print("Select filtered table Videos failed. Error is: \(nserror),\(nserror.userInfo)")
            return nil
        }
    }
    
    func Trending() -> AnySequence<Row>? {
        do{
            // select * from "shows" where ("id" = 1)
            // let filterCondition = (videoId == 1)
            
            //select * from "shows" where ("id" IN (1,2,3,4))
            //let filterCondition = [1,2,3,4].contains(id)
            
            //select * from "shows" where ("name" LIKE '%Onstage')
            //let filterCondition = self.name.like('%Onstage')
            
            //select * from "shows" where name.lowercaseString == "onstage" and id >= 3
            //let filterCondition = (id >= 3) && (name.lowercaseString == "onstage")
            
            //select * from "shows" where ("id" == 3) OR ("id" == 3)
            
            var filterCondition = (isActive == "True") && (isTrending == "True")
            
            return try DBHelper.shared.connection?.prepare(self.tblVideos.filter(filterCondition))
        }
        catch{
            let nserror = error as NSError
            print("Select filtered table Videos failed. Error is: \(nserror),\(nserror.userInfo)")
            return nil
        }
    }
    
    func filter(type: String) -> AnySequence<Row>? {
        do{
            // select * from "shows" where ("id" = 1)
           // let filterCondition = (videoId == 1)
            
            //select * from "shows" where ("id" IN (1,2,3,4))
            //let filterCondition = [1,2,3,4].contains(id)
            
            //select * from "shows" where ("name" LIKE '%Onstage')
            //let filterCondition = self.name.like('%Onstage')
            
            //select * from "shows" where name.lowercaseString == "onstage" and id >= 3
            //let filterCondition = (id >= 3) && (name.lowercaseString == "onstage")
            
            //select * from "shows" where ("id" == 3) OR ("id" == 3)
            
            var filterCondition = (isActive == "True")
            
            if type == "News" {
                filterCondition = (isActive == "True") && (showId == 90 || showId == 89)
            }
            else if type == "Catchup" {
                filterCondition = (isActive == "True") && (isCatchup == "True") && (showId != 90 && showId != 89)
            }
            else if type == "Trending" {
                filterCondition = (isActive == "True") && (isTrending == "True")
            }
            else if type == "Documentary" {
                filterCondition = (isActive == "True") && (categoryId == 15 || categoryId == 9)
            }
            
            return try DBHelper.shared.connection?.prepare(self.tblVideos.filter(filterCondition).order(videoId.desc, videoId))
        }
        catch{
            let nserror = error as NSError
            print("Select filtered table Videos failed. Error is: \(nserror),\(nserror.userInfo)")
            return nil
        }
    }
    
    func searchVideos(type: String) -> AnySequence<Row>? {
        do{
            // select * from "shows" where ("id" = 1)
            // let filterCondition = (videoId == 1)
            
            //select * from "shows" where ("id" IN (1,2,3,4))
            //let filterCondition = [1,2,3,4].contains(id)
            
            //select * from "shows" where ("name" LIKE '%Onstage')
            //let filterCondition = self.name.like('%Onstage')
            
            //select * from "shows" where name.lowercaseString == "onstage" and id >= 3
            //let filterCondition = (id >= 3) && (name.lowercaseString == "onstage")
            
            //select * from "shows" where ("id" == 3) OR ("id" == 3)
            
            var filterCondition = self.title.like("%%" + type + "%%") || self.showName.like("%%" + type + "%%")
            
            return try DBHelper.shared.connection?.prepare(self.tblVideos.filter(filterCondition).order(videoId.desc, videoId))
        }
        catch{
            let nserror = error as NSError
            print("Select searchVideo table Videos failed. Error is: \(nserror),\(nserror.userInfo)")
            return nil
        }
    }
    
    func toString(video: Row) {
       /*  print("""
            Video details. videoId = \(video[self.videoId]),\
            categoryId = \(video[self.categoryId]),
            showId = \(video[self.showId]),
            thumb = \(video[self.thumb]),
            url = \(video[self.url]),
            title = \(video[self.title]),
            shortDesc = \(video[self.shortDesc]),
            longDesc = \(video[self.longDesc]),
            isActive = \(video[self.isActive]),
            isTrending = \(video[self.isTrending]),
            parent = \(video[self.parent]),
            likes = \(video[self.likes]),
            views = \(video[self.views]),
            isCatchup = \(video[self.isCatchup]),
            expDate = \(video[self.expDate]),
            showName = \(video[self.showName])
            """) */
    }
    
    func toObject(video: Row) -> Video {
        
        let video_ = Video(videoId: Int(video[self.videoId]), categoryId: Int(video[self.categoryId]), showId: Int(video[self.showId]), thumb: video[self.thumb], url: video[self.url], title: video[self.title], shortDesc: video[self.shortDesc], longDesc: video[self.longDesc], isAvtive: video[self.isActive], isTrending: video[self.isTrending], parent: video[self.parent], likes: video[self.likes], views: video[self.views], isCatchup: video[self.isCatchup], expDate: video[self.expDate], showName: video[self.showName], latestVideo: video[self.latestVideo])
        
        return video_!
    }
}

