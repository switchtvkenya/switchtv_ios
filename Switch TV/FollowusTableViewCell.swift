//
//  FollowusTableViewCell.swift
//  Switch TV
//
//  Created by Switch TV Kenya on 03/04/2019.
//  Copyright © 2019 Switch TV. All rights reserved.
//

import UIKit

class FollowusTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabelHandle: UILabel!
    @IBOutlet weak var titleLabelFollow: UILabel!
    @IBOutlet weak var imageViewFollow: UIImageView!
    
    func setFollow(icon: UIImage, title: String, handle: String) {
        
        imageViewFollow.image = icon
        titleLabelFollow.text = title
        titleLabelHandle.text = handle
        
    }
}
