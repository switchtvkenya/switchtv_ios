//
//  YTCollectionViewCell3.swift
//  Switch TV
//
//  Created by Switch TV Kenya on 27/03/2019.
//  Copyright © 2019 Switch TV. All rights reserved.
//

import UIKit
import Nuke

class YTCollectionViewCell3: UICollectionViewCell {
    
    @IBOutlet weak var YTImageView3: UIImageView!
    @IBOutlet weak var YTLabel3: UILabel!
    
    func setYTVideo(video: Video) {
        let url = URL(string: video.thumb)
        YTImageView3.image = nil
        // let width = UIScreen.main.bounds.size.width
        // let height = UIScreen.main.bounds.size.height
        let request = ImageRequest(
            url: url!,
            targetSize: CGSize(width: 500, height: 300),
            //   targetSize: CGSize(width: width, height: height),
            contentMode: .aspectFit)
        Nuke.loadImage(with: request, into: YTImageView3)
        //  Nuke.loadImage(with: url!, into: imageView)
        
        YTLabel3.text = video.title
        // video_ = video
        // setTapable()
        
    }
    
}
