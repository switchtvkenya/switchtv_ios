//
//  LiveCollectionViewCell2.swift
//  Switch TV
//
//  Created by Switch TV Kenya on 02/04/2019.
//  Copyright © 2019 Switch TV. All rights reserved.
//

import UIKit
import Nuke

class LiveCollectionViewCell2: UICollectionViewCell {
    
    @IBOutlet weak var imageViewLive2: UIImageView!
    @IBOutlet weak var titleLabelLive2: UILabel!
    
    func setTrendingVideo(video: Video) {
        let url = URL(string: video.thumb)
        imageViewLive2.image = nil
        // let width = UIScreen.main.bounds.size.width
        // let height = UIScreen.main.bounds.size.height
        let request = ImageRequest(
            url: url!,
            targetSize: CGSize(width: 500, height: 300),
            //   targetSize: CGSize(width: width, height: height),
            contentMode: .aspectFit)
        Nuke.loadImage(with: request, into: imageViewLive2)
        //  Nuke.loadImage(with: url!, into: imageView)
        
        titleLabelLive2.text = video.showName + ": " + video.shortDesc
    }
}
