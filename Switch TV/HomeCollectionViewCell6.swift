//
//  HomeCollectionViewCell6.swift
//  Switch TV
//
//  Created by Switch TV Kenya on 05/04/2019.
//  Copyright © 2019 Switch TV. All rights reserved.
//

import UIKit
import Nuke

class HomeCollectionViewCell6: UICollectionViewCell {
    @IBOutlet weak var picha: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    func setHomeVideo(video: Video) {
        let url = URL(string: video.thumb)
        picha.image = nil
        // let width = UIScreen.main.bounds.size.width
        // let height = UIScreen.main.bounds.size.height
        let request = ImageRequest(
            url: url!,
            targetSize: CGSize(width: 500, height: 300),
            //   targetSize: CGSize(width: width, height: height),
            contentMode: .aspectFit)
        Nuke.loadImage(with: request, into: picha)
        //  Nuke.loadImage(with: url!, into: imageView)
        
        titleLabel.text = video.shortDesc
      //  showLabelEvents.text = video.showName
        
    }
    
}
