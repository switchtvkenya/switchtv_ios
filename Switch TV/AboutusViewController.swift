//
//  AboutusViewController.swift
//  Switch TV
//
//  Created by Switch TV Kenya on 03/04/2019.
//  Copyright © 2019 Switch TV. All rights reserved.
//

import UIKit
import SafariServices

class AboutusViewController: UIViewController, SFSafariViewControllerDelegate {

    @IBOutlet weak var backImageView: UIImageView!
    @IBOutlet weak var tableAbout: UITableView!
    
    let titles = ["About","Legal"]
    let body = ["We will entertain you with the hip shows, movies, series, soaps, news and sports.","Terms and Conditions"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableAbout.delegate = self
        tableAbout.dataSource = self
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        backImageView.isUserInteractionEnabled = true
        backImageView.addGestureRecognizer(tapGestureRecognizer)
        
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let tappedImage = tapGestureRecognizer.view as! UIImageView
        
        // Your action
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }

}

extension AboutusViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AboutTableViewCell") as! AboutTableViewCell
        
        cell.setAbout(title: titles[indexPath.row], descr: body[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 1 {
            let termsUrl = URL(string: "https://www.switchtv.ke/tandc.aspx")
            let safariVC = SFSafariViewController(url: termsUrl!)
            safariVC.delegate = self
            present(safariVC, animated: true, completion: nil)
        }
    }
    
}
