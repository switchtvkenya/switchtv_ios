//
//  HomeViewController.swift
//  Switch TV
//
//  Created by Switch TV on 03/12/2018.
//  Copyright © 2018 Switch TV. All rights reserved.
//

import UIKit
import SQLite
//import Nuke
//import Toucan
import GoogleMobileAds

//class HomeViewController: UIViewController,GADBannerViewDelegate{
class HomeViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UISearchBarDelegate {
    @IBOutlet weak var bannerView: GADBannerView!
    
    @IBOutlet weak var scroll: UIScrollView!
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var feturedRow: UICollectionView!
    @IBOutlet weak var catchupRow: UICollectionView!
    @IBOutlet weak var newsRow: UICollectionView!
    @IBOutlet weak var eventsRow: UICollectionView!
    @IBOutlet weak var showsRow: UICollectionView!
    
    @IBOutlet weak var nav: UINavigationBar!
    @IBOutlet weak var testRow: UICollectionView!
    @IBOutlet weak var menuImage: UIImageView!
    
    @IBOutlet weak var homeTopScroll: NSLayoutConstraint!
    @IBOutlet weak var homeTopLabel: NSLayoutConstraint!
    var videos_catchup: [Video] = []
    var Shows_: [Show] = []
    var videos_documentary: [Video] = []
    var videos_trending: [Video] = []
    var videos_news: [Video] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
       // Utiliity().checkInternet()
        
      //  self.bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        self.bannerView.adUnitID = "ca-app-pub-8457459103891847/5584271465"
        self.bannerView.rootViewController = self
        bannerView.load(GADRequest())
       // GADRequest().testDevices = kGADSimulatorID as? [Any]
        bannerView.delegate = self as? GADBannerViewDelegate
        
    //    scroll.contentSize = CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height)
        
        let modelName = UIDevice.modelName
        print (modelName)
        
        if (modelName.contains("X") || modelName.contains("11") || modelName.contains("12"))
        {
            homeTopLabel.constant = 8
            homeTopScroll.constant = 40
        }
        
        setUpSearchBar()
        
        videos_catchup = catchup()
        videos_trending = trending()
        Shows_ = shows()
        videos_documentary = documentary()
        videos_news = news()
        
        if videos_news.count > 0 {
            newsRow.delegate = self
            newsRow.dataSource = self
        }
        
        if videos_trending.count > 0 {
            feturedRow.delegate = self
            feturedRow.dataSource = self
        }
        
        if videos_catchup.count > 0 {
            catchupRow.delegate = self
            catchupRow.dataSource = self
        }
        
        if videos_documentary.count > 0 {
            eventsRow.delegate = self
            eventsRow.dataSource = self
        }
        
        if Shows_.count > 0 {
            showsRow.delegate = self
            showsRow.dataSource = self
        }
        
    /*    if videos_news.count > 0 {
            testRow.delegate = self
            testRow.dataSource = self
        } */
        
        var refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.testRefresh(_:)), for: .valueChanged)
        refreshControl.tintColor = UIColor.white
        scroll.addSubview(refreshControl)
        
        view.addSubview(scroll)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        menuImage.isUserInteractionEnabled = true
        menuImage.addGestureRecognizer(tapGestureRecognizer)
        
    }
    
    @objc func testRefresh(_ refreshControl: UIRefreshControl?) {
      //  refreshControl?.attributedTitle = NSAttributedString(string: "Refreshing...")
        
        let api = APItoApp()
        api.putVideos()
        
     //   refreshControl?.tintColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 1)
     //s   refreshControl?.tintColor = UIColor.white
        
        DispatchQueue.global(qos: .default).async(execute: {
            
            Thread.sleep(forTimeInterval: 15) //for 3 seconds, prevent scrollview from bouncing back down (which would cover up the refresh view immediately and stop the user from even seeing the refresh text / animation)
            
            
            DispatchQueue.main.async(execute: {
             /*   let formatter = DateFormatter()
                formatter.dateFormat = "MMM d, h:mm a"
                let lastUpdate = "Last updated on \(formatter.string(from: Date()))"
                
                refreshControl?.attributedTitle = NSAttributedString(string: lastUpdate) */
                
                self.videos_catchup = self.catchup()
                self.videos_trending = self.trending()
                self.Shows_ = self.shows()
                self.videos_documentary = self.documentary()
                self.videos_news = self.news()
                
                if self.videos_news.count > 0 {
                    self.newsRow.delegate = self
                    self.newsRow.dataSource = self
                }
                
                if self.videos_trending.count > 0 {
                    self.feturedRow.delegate = self
                    self.feturedRow.dataSource = self
                }
                
                if self.videos_catchup.count > 0 {
                    self.catchupRow.delegate = self
                    self.catchupRow.dataSource = self
                }
                
                if self.videos_documentary.count > 0 {
                    self.eventsRow.delegate = self
                    self.eventsRow.dataSource = self
                }
                
            /*    if self.Shows_.count > 0 {
                    self.showsRow.delegate = self
                    self.showsRow.dataSource = self
                } */
                
                refreshControl?.endRefreshing()
                
                print("refresh end")
            })
        })
    }
    
  /*  @objc func refresh(refreshControl: UIRefreshControl) {
       // scroll.reloadData()
        
        let api = APItoApp()
        api.putVideos()
        
        videos_catchup = catchup()
        videos_trending = trending()
        Shows_ = shows()
        videos_documentary = documentary()
        videos_news = news()
        
        self.catchupRow.reloadData()
        self.newsRow.reloadData()
        self.eventsRow.reloadData()
        self.feturedRow.reloadData()
        self.showsRow.reloadData()
        
        refreshControl.endRefreshing()
    } */
    
    override func viewDidAppear(_ animated: Bool) {
        
        videos_catchup = catchup()
        videos_trending = trending()
        Shows_ = shows()
        videos_documentary = documentary()
        videos_news = news()
        
        if videos_news.count > 0 {
            newsRow.delegate = self
            newsRow.dataSource = self
        }
        
        if videos_trending.count > 0 {
            feturedRow.delegate = self
            feturedRow.dataSource = self
        }
        
        if videos_catchup.count > 0 {
            catchupRow.delegate = self
            catchupRow.dataSource = self
        }
        
        if videos_documentary.count > 0 {
            eventsRow.delegate = self
            eventsRow.dataSource = self
        }
        
        if Shows_.count > 0 {
            showsRow.delegate = self
            showsRow.dataSource = self
        }
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let tappedImage = tapGestureRecognizer.view as! UIImageView
        
        // Your action
       // self.performSegue(withIdentifier: "SttSegue_Home", sender: nil)
        self.performSegue(withIdentifier: "DailyMotionSegue_Home", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "VimeoSegue_Home" {
            let vimeoVC = segue.destination as! VimeoViewController
            vimeoVC.video = sender as? Video
        }
        else if segue.identifier == "YTSegue_Home" {
            let vimeoVC = segue.destination as! YTViewController
            vimeoVC.video = sender as? Video
        }
        else if segue.identifier == "ShowsSegue_Home" {
            let showsVC = segue.destination as! showLandingViewController
            showsVC.show = sender as? Show
        }
        else if segue.identifier == "SttSegue_Home" {
            let sttVC = segue.destination as! SettingsViewController
        }
        else if segue.identifier == "SearchSegue_Home" {
            let searchVC = segue.destination as! SearchResultViewController
            searchVC.serchPhrase = sender as? String
        }
    }
    
    func catchup() -> [Video] {
        
        var tempVideos: [Video] = []
        // retrieve schedule from sqlite
        if let videoQuery: AnySequence<Row> = VideoEntity.shared.getCatchup(){
            for eachVideo in videoQuery{
                var video1 = VideoEntity.shared.toObject(video: eachVideo)
                tempVideos.append(video1)
            }
        }
        
        return tempVideos
        
    }
    
    func setUpSearchBar()
    {
        searchBar.delegate = self
        
    }
    
    func trending() -> [Video] {
        
        var tempVideos: [Video] = []
        // retrieve schedule from sqlite
        if let videoQuery: AnySequence<Row> = VideoEntity.shared.filter(type: "Trending") {
            for eachVideo in videoQuery{
                var video1 = VideoEntity.shared.toObject(video: eachVideo)
                tempVideos.append(video1)
            }
        }
        
        return tempVideos
        
    }
    
    func news() -> [Video] {
        
        var tempVideos: [Video] = []
        // retrieve schedule from sqlite
        if let videoQuery: AnySequence<Row> = VideoEntity.shared.filter(type: "News") {
            for eachVideo in videoQuery{
                var video1 = VideoEntity.shared.toObject(video: eachVideo)
                tempVideos.append(video1)
            }
        }
        
        return tempVideos
        
    }
    
    func documentary() -> [Video] {
        
        var tempVideos: [Video] = []
        // retrieve schedule from sqlite
        if let videoQuery: AnySequence<Row> = VideoEntity.shared.filter(type: "Documentary"){
            for eachVideo in videoQuery{
                var video1 = VideoEntity.shared.toObject(video: eachVideo)
                tempVideos.append(video1)
            }
        }
        
        return tempVideos
        
    }
    
    func shows() -> [Show] {
        var tempShows: [Show] = []
        
        if let showQuery: AnySequence<Row> = ShowEntity.shared.queryAll(){
            for eachShow in showQuery{
                let shows1 = ShowEntity.shared.toObject(shows: eachShow)
                tempShows.append(shows1)
            }
        }
        
        return tempShows
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == self.feturedRow {
            return 4
        }
        else if collectionView == self.catchupRow {
            return videos_catchup.count
        }
        else if collectionView == self.showsRow {
            return Shows_.count
          //  return 15
        }
        else{
            return 8
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.feturedRow {
            let cell:HomeCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath) as! HomeCollectionViewCell
            
            let video_ = videos_trending[indexPath.row]
            cell.setHomeVideo(video: video_)
            return cell
            
        }
        else if collectionView == self.catchupRow {
            let cell:HomeCollectionViewCell2 = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell2", for: indexPath) as! HomeCollectionViewCell2
            
            let video_ = videos_catchup[indexPath.row]
            cell.setHomeVideo(video: video_)
            
            return cell
        }
            
        else if collectionView == self.newsRow {
            let cell:HomeCollectionViewCell3 = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell3", for: indexPath) as! HomeCollectionViewCell3
            
            let video_ = videos_news[indexPath.row]
            cell.setHomeVideo(video: video_)
            
            return cell
        }
            
        else if collectionView == self.eventsRow {
            let cell:HomeCollectionViewCell4 = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell4", for: indexPath) as! HomeCollectionViewCell4
            
            let video_ = videos_documentary[indexPath.row]
            cell.setHomeVideo(video: video_)
            
            return cell
        }
        else if collectionView == self.showsRow {
            let cell:HomeCollectionViewCell5 = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell5", for: indexPath) as! HomeCollectionViewCell5
            
            let show_ = Shows_[indexPath.row]
            cell.setShows(shows: show_)
            
            return cell
        }
            
        else {
            let cell:HomeCollectionViewCell6 = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell6", for: indexPath) as! HomeCollectionViewCell6
            
            let video_ = videos_news[indexPath.row]
            cell.setHomeVideo(video: video_)
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
     //   print ("clicked Home " + String(indexPath.row))
        
        if collectionView == self.showsRow {
            // perform segue
            let show_ = Shows_[indexPath.row]
            self.performSegue(withIdentifier: "ShowsSegue_Home", sender: show_)
        }
            
        else {
            // perform segue
            var video_ : Video?
            
            if collectionView == self.feturedRow {
                video_ = videos_trending[indexPath.row]
            }
            else if collectionView == self.catchupRow {
                video_ = videos_catchup[indexPath.row]
            }
            else if collectionView == self.newsRow {
                video_ = videos_news[indexPath.row]
            }
            else if collectionView == self.eventsRow {
                video_ = videos_documentary[indexPath.row]
            }
            else if collectionView == self.testRow {
                video_ = videos_news[indexPath.row]
            }
            
            if video_!.url.contains("https") {
                self.performSegue(withIdentifier: "VimeoSegue_Home", sender: video_)
            }
            else {
                self.performSegue(withIdentifier: "YTSegue_Home", sender: video_)
            }
        }
    }
    
    // search Bar function
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print (searchBar.text!)
        self.performSegue(withIdentifier: "SearchSegue_Home", sender: searchBar.text)
    }
    
  /*  /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("adViewDidReceiveAd")
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
                didFailToReceiveAdWithError error: GADRequestError) {
        print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
        print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
        print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
        print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
        print("adViewWillLeaveApplication")
    }
    */
}

/* extension HomeViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell", for: indexPath) as? HomeTableViewCell
        cell?.registerCollectionView(datasource: self)
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Home " + String(indexPath.row))
    }
    
}

extension HomeViewController:UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return videos_catchup.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath) as? HomeCollectionViewCell
        
       // cell!.frame.size = CGSize(width: 240, height: 190)
        
       // cell?.imageView.frame.size = CGSize(width: 200, height: 128)
        
        cell?.imageView.frame.size = CGSize(width: cell!.frame.size.width , height: cell!.frame.size.height)
        
        //  cell.showImageView.layer.borderWidth = 1
        // cell.showImageView.layer.masksToBounds = false
        // cell.showImageView.layer.borderColor = UIColor.black.cgColor
        // cell.showImageView.layer.cornerRadius = cell.showImageView.frame.height/10
        
      //  cell!.imageView.layer.cornerRadius = cell!.imageView.frame.height/14
      //  cell!.imageView.clipsToBounds = true
        
        let video_catchup = videos_catchup[indexPath.row]
        
        cell?.setVideo(video: video_catchup)
        
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Home " + String(indexPath.row))
    }
    
} */



