//
//  BMCustomPlayer.swift
//  Switch TV
//
//  Created by Switch TV Kenya on 09/04/2019.
//  Copyright © 2019 Switch TV. All rights reserved.
//

import Foundation
import BMPlayer

class BMCustomPlayer: BMPlayer {
    override func storyBoardCustomControl() -> BMPlayerControlView? {
        return BMPlayerCustomControlView()
    }
}
