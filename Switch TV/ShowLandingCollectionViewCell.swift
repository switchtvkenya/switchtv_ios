//
//  ShowLandingCollectionViewCell.swift
//  Switch TV
//
//  Created by Switch TV Kenya on 14/03/2019.
//  Copyright © 2019 Switch TV. All rights reserved.
//

import UIKit
import Nuke

protocol ShowLandingCollectionViewCellDelegate {
    func didSelectVideo(video_: Video)
}

class ShowLandingCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var labelDescr: UILabel!
    
    var video_: Video!
    var delegate: ShowLandingCollectionViewCellDelegate?
    
    func setTapable()
    {
    let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
    imageView.isUserInteractionEnabled = true
    imageView.addGestureRecognizer(tapGestureRecognizer)
    }

@objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
{
    let tappedImage = tapGestureRecognizer.view as! UIImageView
    
    // Your action
   // print("Back")
    
 //   navigationController?.popViewController(animated: true)
    
 //   dismiss(animated: true, completion: nil)
    
    delegate?.didSelectVideo(video_: video_)
}
    
    func setShowVideo(video: Video) {
        let url = URL(string: video.thumb)
        imageView.image = nil
        // let width = UIScreen.main.bounds.size.width
        // let height = UIScreen.main.bounds.size.height
        let request = ImageRequest(
            url: url!,
            targetSize: CGSize(width: 500, height: 300),
            //   targetSize: CGSize(width: width, height: height),
            contentMode: .aspectFit)
        Nuke.loadImage(with: request, into: imageView)
        //  Nuke.loadImage(with: url!, into: imageView)
        
        labelDescr.text = video.title
        video_ = video
        setTapable()
        
    }
    
}
