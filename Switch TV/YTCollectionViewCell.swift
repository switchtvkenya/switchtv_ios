//
//  YTCollectionViewCell.swift
//  Switch TV
//
//  Created by Switch TV Kenya on 22/03/2019.
//  Copyright © 2019 Switch TV. All rights reserved.
//

import UIKit
import Nuke

class YTCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var YTTitleLabel: UILabel!
    @IBOutlet weak var YTImageView: UIImageView!
    
    func setYTVideo(video: Video) {
        let url = URL(string: video.thumb)
        YTImageView.image = nil
        // let width = UIScreen.main.bounds.size.width
        // let height = UIScreen.main.bounds.size.height
        let request = ImageRequest(
            url: url!,
            targetSize: CGSize(width: 500, height: 300),
            //   targetSize: CGSize(width: width, height: height),
            contentMode: .aspectFit)
        Nuke.loadImage(with: request, into: YTImageView)
        //  Nuke.loadImage(with: url!, into: imageView)
        
        YTTitleLabel.text = video.title
       // video_ = video
       // setTapable()
        
    }
}
