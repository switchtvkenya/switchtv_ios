//
//  Show.swift
//  Switch TV
//
//  Created by Switch TV Kenya on 21/02/2019.
//  Copyright © 2019 Switch TV. All rights reserved.
//

import Foundation
import UIKit

class Show {
    
    //MARK: Properties
    var id: Int
    var name: String
    var description: String
    var category: String
    var hasCatchup: String
    var showTime: String
    var showSchedule: String
    var showPriority: Int
    // var photo: UIImage?
    
    init?(id: Int, name: String, description: String, category: String, hasCatchup: String,  showTime: String, showSchedule: String, showPriority: Int) {
        
        // Initialization should fail if there is no name or if the rating is negative.
        /*   if videoId == 0 || categoryId == 0 || showId == 0 || thumb.isEmpty  {
         return nil
         } */
        
        // Initialize stored properties.
        self.id = id
        self.name = name
        self.description = description
        self.category = category
        self.hasCatchup = hasCatchup
        self.showTime = showTime
        self.showSchedule = showSchedule
        self.showPriority = showPriority
    }
    
}
