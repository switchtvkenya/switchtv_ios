//
//  NewsCell.swift
//  Switch TV
//
//  Created by Switch TV Kenya on 14/02/2019.
//  Copyright © 2019 Switch TV. All rights reserved.
//

import UIKit
import Nuke

class NewsCell: UITableViewCell {

    @IBOutlet weak var videoImageView: UIImageView!
    @IBOutlet weak var videoTitleLabel: UILabel!
    @IBOutlet weak var videoDescrLabel: UILabel!
    
    func setNewsVideo(video: Video) {
        let url = URL(string: video.thumb)
        videoImageView.image = nil
        let request = ImageRequest(
            url: url!,
            targetSize: CGSize(width: videoImageView.frame.width, height: videoImageView.frame.width),
            contentMode: .aspectFill)
        Nuke.loadImage(with: request, into: videoImageView)
       // Nuke.loadImage(with: url!, into: videoImageView)
        
        videoTitleLabel.text = video.title
        videoDescrLabel.text = video.shortDesc
        
    }
    
}
