//
//  HomeTableViewCell.swift
//  Switch TV
//
//  Created by Switch TV Kenya on 19/02/2019.
//  Copyright © 2019 Switch TV. All rights reserved.
//

import UIKit

class HomeTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func registerCollectionView<DataSource:UICollectionViewDataSource>(datasource: DataSource) {
        self.collectionView.dataSource = datasource
    }

}
